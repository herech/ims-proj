///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Store.hpp                                                  ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef STORE_HPP_INCLUDED
#define STORE_HPP_INCLUDED


#include "SHOlib.hpp"


#include <string>
#include <vector>


using namespace std;

/**
 * Trida reprezentujici aktualne obsluhovanou transakci
 */
class CurrentlyServicedTransaction {
    public:
        void* object_ptr;
        double service_time_begin;

        CurrentlyServicedTransaction (void* object_ptr, double service_time_begin);

};


/**
 * Trida obsluzne linky
 */
class Store {
    private:
        int capacity;                 // aktualni kapacita obsluzne linky
        int default_capacity;         // vychozi kapacita
        int max_capacity;             // maximalni kapacita obsluzne linky
        int last_capacity = 0;            // posledni kapacita (po ukonceni simulace - kvuli statistikam)
        string name;                  // jmeno obsluzne linky
        vector<Queue*> queue_vector;  // seznam front pro danou obsluznou linku serazenych podle priority

        // staticka datova struktura uchovavajici seznam vsech obsluznych linek v programu
        static vector<Store*> all_stores;

        typedef Store self;  // alias pro tridu Store pouzitelny uvnitr teto tridy

        vector<CurrentlyServicedTransaction*> currently_serviced_transactions;  // seznam aktualne obsluhovanych transakci
        Stat *stat_service_time = new Stat("Doba obsluhy"); // statistika pro dobu obsluhy linky
        Stat *stat_capacity_before = new Stat("Zabrana kapacita pred enter"); // statistika pro dobu obsluhy linky
        Stat *stat_capacity_after = new Stat("Zabrana kapacita po enter"); // statistika pro dobu obsluhy linky

        int incoming = 0;
        int outcoming = 0;
        int enter = 0;
        int leave = 0;

    public:
        /**
         * Konstruktor tridy Store
         * @param  name          Jmeno obsluzne linky
         * @param  capacity      Aktualni kapacita obsluzne linky
         * @param  max_capacity  Maximalni kapacita obsluzne linky
         */
        Store (string name, int capacity = 0, int max_capacity = INFINITE_CAPACITY);

        /**
         * Pretizeny konstruktor tridy Store
         * - explicitne nastavujeme frontu
         * @param   name                    Jmeno obsluzne linky
         * @param  *highest_priority_front  Ukazatel na frontu pro tuto obsluznou linku
         * @param   capacity                Aktualni kapacita obsluzne linky
         * @param   max_capacity            Maximalni kapacita obsluzne linky
         */
        Store (string name, Queue *highest_priority_front, int capacity = 0, int max_capacity = INFINITE_CAPACITY);

        /**
         * Metoda pro pridani fronty na konec seznamu front
         * - povazovana za frontu s nejmensi prioritou
         * @param  *new_queue  Ukazatel na frontu pro tuto obsluznou linku
         */
        void addQueue(Queue *new_queue);

        /**
         * Metoda pro zabrani obsluzne linky
         * - pokud ji nemuze zabrat, jde do fronty
         * @param   quantity                 Pozadovana kapacita k zabrani
         * @param  *next_function_ptr        Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param  *object_ptr               Ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
         * @param   priority                 Priorita pro razeni ve fronte
         * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
         * @param  *queue_to_wait            Ukazatel na frontu u obsluzne linky
         */
        void Enter (int quantity, void (*function_ptr)(void*), void* object_ptr, int priority, void (*last_event_function_ptr)(void*), Queue* queue_to_wait = NULL);

        /**
         * Metoda pro uvolneni obsluzne linky
         * - uvolni zadanou kapacitu
         * @param   quantity  Pocet, jakou kapacitu chceme uvolnit
         */
        void Leave (int quantity, void* object_ptr);

        /**
         * Metoda pro zjisteni, zda je obsluzna linka prazdna
         * @return  true   Obsluzna linka je prazdna
         * @return  false  Obsluzna linka neni prazdna
         */
        bool Empty ();

        /**
         * Metoda pro reinicializaci vsech storu
         */
        static void reinitializeAllStores();

        /**
         * Metoda pro vypis statistik
         */
        void Output();

        /**
         * Metoda pro smazani zaznamenanych hodnot v ramci statistiky fronty
         */
        void Delete();

        /**
         * Metoda pro smazani/reinicializaci zaznamenanych hodnot ve statistice fronty
         */
        static void Clear();

        /**
         * Metoda vrati hlavni frontu pro dany store
         */
        Queue* GetQueue();

        /**
         * Metoda vrati volnou kapacitu daneho store
         */
        int GetCapacity();

        /**
         * Metoda nastavi volnou kapacitu daneho store
         */
        void SetCapacity(int capacity);
};

#endif // STORE_HPP_INCLUDED
