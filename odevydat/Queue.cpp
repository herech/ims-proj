///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Queue.cpp                                                  ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


/**
 * Zdroj iteratoru
 * http://stackoverflow.com/questions/12702561/c-iterate-through-vector-using-for-loop
 */


#include "SHOlib.hpp"


#include <iomanip>
#include <sstream>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


using namespace std;


// inicializace staticke datove struktury uchovavajici seznam vsech front v systemu
vector<Queue*> Queue::all_queues;


/**
 * Konstruktor tridy Queue
 * @param  name  Jmeno fronty
 */
Queue::Queue(string name) {
    this->name = name;                   // nastavi jmeno fronty

    // inicializace pocitadel
    incoming  = 0;
    outcoming = 0;

    frontLengthAfter = new Stat(this->name);  // statistika delky fronty po prichodu do fronty
    frontLengthBefore = new Stat(this->name);  // statistika delky fronty pred prichodem do fronty
    frontTime = new Stat(this->name);    // statistika cekani ve fronte

    self::all_queues.push_back(this);    // prida tuto frontu do seznamu vsech front
}


/**
 * Pretizeny konstruktor tridy Queue
 * - nezadavame jmeno fronty pri inicializaci
 */
Queue::Queue() {
    this->name = "";

    // inicializace pocitadel
    incoming  = 0;
    outcoming = 0;

    frontLengthAfter = new Stat(this->name);  // statistika delky fronty po prichodu do fronty
    frontLengthBefore = new Stat(this->name);  // statistika delky fronty pred prichodem do fronty
    frontTime = new Stat(this->name);    // statistika cekani ve fronte

    self::all_queues.push_back(this);    // prida tuto frontu do seznamu vsech front
}


/**
 * Metoda pro vlozeni prvku do fronty
 * - fronta je vzdy serazena podle priority udalosti a casu prichodu do fronty (FIFO priority Queue)
 * - pri vkladani hleda a vklada na spravne misto
 * @param  queue_item  Prvek fronty reprezentujici udalost, ktera ma jit cekat do fronty
 */
void Queue::Insert(QueueItem queue_item) {
    // iterator pres prvky fronty
    auto it = begin (this->waiting_transactions);

    // prochazime prvky fronty a hledame spravne misto, kam vlozit prvek
    for (; it != end (this->waiting_transactions); ++it) {
        // pokud ma vkladany prvek vetsi prioritu nez prvek z fronty
        if (it->priority < queue_item.priority) {
            // ukoncime hledani a vlozime prvek do fronty
            break;
        }
    }

    // vlozim pocet udalosti ve fronte pred prichodem transakce do statistiky
    frontLengthBefore->Add(this->incoming - this->outcoming);

    // inkrementuju prichozi udalosti
    (this->incoming)++;

    // vlozim pocet udalosti ve fronte po prichodu transakce do statistiky
    frontLengthAfter->Add(this->incoming - this->outcoming);

    // ulozim cas, kdy se udalost zaradila do fronty
    queue_item.start_time = Simulation::Time();

    // vlozime prvek fronty na spravne misto
    this->waiting_transactions.insert(it, queue_item);
}


/**
 * Metoda pro zjisteni, zda je fronta prazdna
 * @return  true   Fronta je prazdna
 * @return  false  Fronta neni prazdna
 */
bool Queue::Empty() {
    return this->waiting_transactions.empty();
}


/**
 * Metoda pro ziskani prvniho prvku z fronty
 * @return  QueueItem  Prvni prvek ve fronte
 */
QueueItem Queue::GetFirst() {
    return (this->waiting_transactions).front();
}


/**
 * Metoda pro odstraneni prvniho prvku z fronty
 */
void Queue::DeleteFirst() {
    // inkrementuju odchozi udalosti
    (this->outcoming)++;

    // vlozim cas straveny ve fronte do statistiky
    double time_tmp = (Simulation::Time()) - (this->waiting_transactions.begin()->start_time);
    frontTime->Add(time_tmp);

    this->waiting_transactions.erase(this->waiting_transactions.begin());
}

/**
 * Metoda pro odstraneni nteho prvku z fronty
 */
void Queue::DeleteNth(int n) {
    // inkrementuju odchozi udalosti
    (this->outcoming)++;

    // vlozim cas straveny ve fronte do statistiky
    double time_tmp = (Simulation::Time()) - ((this->waiting_transactions[n]).start_time);
    frontTime->Add(time_tmp);

    this->waiting_transactions.erase(this->waiting_transactions.begin() + n);
}


/**
 * Metoda pro nastaveni jmena fronty
 * @param  name  Jmeno fronty
 */
void Queue::setName(string name) {
    this->name = name;
}


/**
 * Metoda pro ziskani jmena fronty
 * @return  string  Jmeno fronty
 */
string Queue::getName() {
    return this->name;
}

int Queue::getLength() {
    return (this->waiting_transactions).size();
}


/**
 * Staticka metoda pro smazani vsech udalosti cekajicich v nektere z fornt v seznamu vsech front
 */
void Queue::clearAllQueueItems() {
    // iterator pres seznam vsech front systemu
    auto it = begin (self::all_queues);

    // projdeme fronty v seznamu front a smazeme udalosti v nich cekajici
    for (; it != end (self::all_queues); ++it) {
        ((*it)->waiting_transactions).clear();
    }
}


/**
 * Staticka metoda pro reinicializaci statistik
 */
void Queue::Clear() {
    // iterator pres seznam vsech front systemu
    auto it = begin (self::all_queues);

    // projdeme fronty v seznamu front a reinicializujeme potrebne cleny
    for (; it != end (self::all_queues); ++it) {
        ((*it)->incoming)  = 0;
        ((*it)->outcoming) = 0;
        ((*it)->frontLengthAfter)->Clear();
        ((*it)->frontLengthBefore)->Clear();
        ((*it)->frontTime)->Clear();
    }
}


/**
 * Metoda pro vypis statistik pro tuto frontu
 */
void Queue::Output() {
    ostringstream oss_out;  // datovy proud pro vystup

    if (num_queuing_transaction > outcoming) {
       num_queuing_transaction -= outcoming;
    }
    else {
        num_queuing_transaction = 0;
    }

    for (unsigned int i = 0; i < num_queuing_transaction; ++i) {
        frontLengthAfter->Add(0);
        frontLengthBefore->Add(0);
    }

    num_queuing_transaction = 0;

    // formatovani vypisu
    oss_out << '+' << setw(78) << setfill('-') << '+' <<  endl;
    oss_out << '|' << " QUEUE: " << setw(69) << setfill(' ') << left << name << '|' <<  endl;
    oss_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    oss_out << '|' << " Incoming: "  << setw(66) << setfill(' ') << left << incoming << '|' <<  endl;
    oss_out << '|' << " Outcoming: " << setw(65) << setfill(' ') << left << outcoming << '|' <<  endl;

    oss_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    oss_out << '|' << " Minimal length: " << setw(60) << setfill(' ') << left << frontLengthBefore->getMinValue() << '|' <<  endl;
    oss_out << '|' << " Maximal length: " << setw(60) << setfill(' ') << left << frontLengthAfter->getMaxValue() << '|' <<  endl;
    oss_out << '|' << " Average length: " << setw(60) << setfill(' ') << left << (frontLengthAfter->getAverageValue() + frontLengthBefore->getAverageValue() ) / 2.0 << '|' <<  endl;
    oss_out << '|' << " Length deviation: " << setw(58) << setfill(' ') << left << (frontLengthAfter->getStdDeviation() + frontLengthBefore->getStdDeviation()) / 2.0 << '|' <<  endl;

    oss_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    oss_out << '|' << " Minimal time: " << setw(62) << setfill(' ') << left << frontTime->getMinValue() << '|' <<  endl;
    oss_out << '|' << " Maximal time: " << setw(62) << setfill(' ') << left << frontTime->getMaxValue() << '|' <<  endl;
    oss_out << '|' << " Average time: " << setw(62) << setfill(' ') << left << frontTime->getAverageValue() << '|' <<  endl;
    oss_out << '|' << " Time deviation: " << setw(60) << setfill(' ') << left << frontTime->getStdDeviation() << '|' <<  endl;

    oss_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    // vystup na stdout nebo do souboru
    Simulation::Output(oss_out.str());
}


void* Queue::removeQueueItemByEvent(void (*last_event_function_ptr)(void*)) {

    auto it = begin (self::all_queues);

    // prochazime vsechyn fronty
    for (; it != end (self::all_queues); ++it) {

        Queue* current_queue = *it;
        unsigned int i = 0;
        for (; i < (current_queue->waiting_transactions).size(); i++) {
            if ((current_queue->waiting_transactions[i]).last_event_function == last_event_function_ptr) {
                void* object_transaction =  (current_queue->waiting_transactions[i]).event_object;      // ulozime si prvek ve frontě
                current_queue->DeleteNth(i);
                //(current_queue->waiting_transactions).erase(begin (current_queue->waiting_transactions) + i);      // smazeme prvek ve fronte
                return object_transaction;                  // vracime prvek ve frontě
            }
               // printf(" >> %p >> %p\n", (void*) (current_queue->waiting_transactions[i]).last_event_function, (void*) last_event_function_ptr);
        }
        /*cout << "SS: " << (current_queue->waiting_transactions).size() << endl;

        auto it2 = begin (current_queue->waiting_transactions);
        // prochazime prvky ve frontě
        for (; it2 != end (current_queue->waiting_transactions); ++it) {
            // pokud se prvek ve fronte nachazi na pozadovanem miste
            if (it2->last_event_function == last_event_function_ptr) {
                void* object_transaction =  (*it2).event_object;      // ulozime si prvek ve frontě
                (current_queue->waiting_transactions).erase(it2);      // smazeme prvek ve fronte
                return object_transaction;                  // vracime prvek ve frontě
            }
            //printf(" >> %p >> %p\n", (void*) it2->last_event_function, (void*) last_event_function_ptr);
            //cout << ">>>>" << it2->last_event_function << ">>>" << last_event_function_ptr << endl;
        }*/

    }

    // neexistuje transakce v zadne fronte - vracime event_object jako NULL
    return NULL;
}

void* Queue::removeQueueItemByEventAndObj(void (*last_event_function_ptr)(void*), void* obj) {

    auto it = begin (self::all_queues);

    // prochazime vsechyn fronty
    for (; it != end (self::all_queues); ++it) {

        Queue* current_queue = *it;
        unsigned int i = 0;
        for (; i < (current_queue->waiting_transactions).size(); i++) {
            if ((current_queue->waiting_transactions[i]).last_event_function == last_event_function_ptr
                && (current_queue->waiting_transactions[i]).event_object == obj) {
                void* object_transaction =  (current_queue->waiting_transactions[i]).event_object;      // ulozime si prvek ve frontě
                current_queue->DeleteNth(i);
                //(current_queue->waiting_transactions).erase(begin (current_queue->waiting_transactions) + i);      // smazeme prvek ve fronte
                return object_transaction;                  // vracime prvek ve frontě
            }
               // printf(" >> %p >> %p\n", (void*) (current_queue->waiting_transactions[i]).last_event_function, (void*) last_event_function_ptr);
        }

    }

    // neexistuje transakce v zadne fronte - vracime event_object jako NULL
    return NULL;
}

/**
 * Metoda pro vraceni poctu prichozich transakci
 */
unsigned int Queue::GetIncoming() {
    return this->incoming;
}
