///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Simulation.hpp                                             ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef SIMULATION_HPP_INCLUDED
#define SIMULATION_HPP_INCLUDED


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


using namespace std;


/**
 * Staticka trida simulace
 */
class Simulation {
    public:
        /**
         * Staticka metoda pro inicializaci simulace
         * - nastavi pocatecni a koncovy modelovy cas simulace
         * @param  model_time_begin  Pocatecni modelovy cas simulace
         * @param  model_time_end    Koncovy modelovy cas simulace
         */
        static void Init(double model_time_begin, double model_time_end);

        /**
         * Staticka metoda pro beh simulace
         * - invokuje metodu nextEventAlgorithm()
         */
        static void Run();

        /**
         * Staticka metoda pro ziskani aktulniho modeloveho casu
         */
        static double Time();

        /**
         * Staticka metoda pro ziskani koncoveho modeloveho casu
         */
        static double EndTime();

        /**
         * Staticka metoda pro nastaveni priznaku zruseni simulace
         */
        static void Cancel();

        /**
         * Metoda pro urceni aktualniho vystupu statistik
         * @param  stat_output  vystup - jmeno souboru nebo STDOUT
         */
        static void SetOutput(string stat_output);

        /**
         * Metoda pro vraceni typu vystupu statistik
         * @return vraceni typu vystupu statistik - nazev souboru nebo STDOUT
         */
        static string GetOutput();

        /**
         * Staticka metoda pro ukonceni simulace
         * - reinicializujeme promenne
         * - uvolni datove struktury
         * - reinicializuje obsluzne linky
         */
        static void End();

        /**
         * Staticka metoda pro smazani (reinicializaci) statistik
         */
        static void Clear();

        /**
         * Metoda pro vypis statistik
         */
        static void Output(string output);

        /**
         * Staticka metoda pro ziskani skutecneho koncoveho modeloveho casu
         */
        static double RealEndTime();

    private:
        /**
         * Staticka metoda pro spusteni next event algoritmu
         * - provadi udalosti naplanovane v kalendari udalosti
         */
        static void nextEventAlgorithm();

        typedef Simulation self;           // alias pro tridu Simulation pouzitelny uvnitr teto tridy
        static double model_time_current;  // tridni promenna pro aktualni modelovy cas
        static double model_time_end;      // tridni promenna pro koncovy modelovy cas
        static double model_time_end_real; // tridni promenna pro skutecny koncovy modelovy cas
        static bool cancel_simulation;     // tridni promenna pro priznak ukonceni simulace
        static string stat_output;         // tridni promenna pro typ vystupu statistik
};

#endif // SIMULATION_HPP_INCLUDED
