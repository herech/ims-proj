///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Histogram.cpp                                              ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#include "SHOlib.hpp"


#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <math.h>
#include <err.h>


using namespace std;


// inicializace staticke datove struktury uchovavajici seznam vsech histogramu v systemu
vector<Histogram*> Histogram::all_histograms;


/**
 * Konstruktor tridy Histogram
 * @param  name            Jmeno histogramu
 * @param  start           Zacatek intervalu
 * @param  step            Krok delici jednotlive tridy
 * @param  num_of_classes  Pocet trid histogramu
 */
Histogram::Histogram (string name, double start, double step, unsigned short num_of_classes) {
    this->name  = name;
    this->start = start;
    this->step  = step;
    this->num_of_classes = num_of_classes;

    statHistogram = new Stat(this->name);   // inicializace statistiky pro histogram

    // inicializace poctu prvku ve tride
    this->count.clear();
    this->count.resize(num_of_classes);
    for (int i = 0; i < this->num_of_classes; ++i) {
        this->count[i] = 0;
    }

    self::all_histograms.push_back(this);   // prida tento histogram do seznamu vsech histogramu
}


/**
 * Metoda pro vlozeni hodnoty do histogramu
 * @param  value  Vkladana hodnota
 */
void Histogram::Add (double value) {
    // vlozi do statistiky
    statHistogram->Add(value);

    // zaradi do tridy
    if ((value >= start) && (value <= (start + num_of_classes * step))) {
        count[int((value - start) / step)]++;
    }
}


/**
 * Metoda pro smazani obsahu histogramu
 * - hodnoty nastavene v kontruktoru zustanou stejne
 */
void Histogram::Delete() {
    statHistogram->Clear();

    // reinicializace poctu prvku ve tride
    this->count.clear();
    this->count.resize(num_of_classes);
    for (int i = 0; i < this->num_of_classes; ++i) {
        this->count[i] = 0;
    }
}


/**
 * Staticka metoda pro reinicializaci vsech histogramu
 */
void Histogram::Clear() {
    // iterator pres seznam vsech histogramu systemu
    auto it = begin (self::all_histograms);

    // projdeme histogramy v seznamu histogramu a reinicializujeme cleny
    for (; it != end (self::all_histograms); ++it) {
        (*it)->Delete();
    }
}


/**
 * Metoda pro vytisknuti statistik z histogramu
 */
void Histogram::Output() {
    double percent;               // procento vyskytu v celku
    double max_count;             // maximalni pocet vyskytu

    ostringstream oss_out;        // datovy proud pro vystup

    int tmp;                      // pomocna promenna pro zobrazeni histogramu
    string str;                   // pomocna promenna pro zobrazeni histogramu


    // spocita maximalni pocet vyskytu
    max_count = count[0];
    for (int i = 0; i < num_of_classes; ++i) {
        max_count = (max_count > count[i]) ? max_count : count[i];
    }


    // formatovani vypisu
    oss_out << '+' << setw(78) << setfill('-') << '+' <<  endl;
    oss_out << '|' << " HISTOGRAM: " << setw(65) << setfill(' ') << left << name << '|' <<  endl;
    oss_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    oss_out << '|' << " Number of records: " << setw(57) << setfill(' ') << left << statHistogram->getNumOfValues() << '|' <<  endl;
    oss_out << '+' << setw(39) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;
    oss_out << '|' << left << " Minimal value: " << setw(22) << setfill(' ') << statHistogram->getMinValue() << right << '|';
    oss_out <<        left << " Average value: " << setw(22) << setfill(' ') << statHistogram->getAverageValue() << right << '|' <<  endl;
    oss_out << '|' << left << " Maximal value: " << setw(22) << setfill(' ') << statHistogram->getMaxValue() << right << '|';
    oss_out <<        left << " Standard deviation: " << setw(17) << setfill(' ') << statHistogram->getStdDeviation() << right << '|' <<  endl;

    oss_out << '+' << setw(16) << setfill('-') << right << '+' << setw(12) << setfill('-') << right << '+'  << setw(11) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;

    oss_out << '|' << left << setw(15) << setfill(' ') << " Range: "  << '|' ;
    oss_out <<        left << setw(11) << setfill(' ') << " Number: "   << '|' ;
    oss_out <<        left << setw(10) << setfill(' ') << " Percent: " << '|' ;
    oss_out <<        left << setw(38) << setfill(' ') << " Graph: "    << '|' <<  endl;
    
    oss_out << '+' << setw(16) << setfill('-') << right << '+' << setw(12) << setfill('-') << right << '+'  << setw(11) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;

    // spocitame kolik # se ma vykreslit pro jednotlive tridy
    max_count = max_count / 35.0;
    for (int i = 0; i < num_of_classes; ++i) {
        tmp = 0;
        str = "";

        percent = count[i] / (statHistogram->getNumOfValues() / 100.0);
        percent = (percent <= 0) ? 0 : percent;

        oss_out << '|' << left  << " " << right << setw(5) << setfill(' ') << (i * step) + start << " - " << left << setw(5) << setfill(' ') << (((i + 1) * step) - 1) + start << " " << '|' ;
        oss_out <<        left  << " " << setw(10)  << setfill(' ') << count[i] << '|' ;
        oss_out <<        right << " " << setw(5)  << setfill(' ') << int(percent) << "%   " << '|' ;

        tmp = count[i] / max_count;
        for (int i = 0; i < tmp; ++i) {
            str += "#";
        }

        oss_out << left << " " << setw(37) << setfill(' ') << str.c_str() << '|' <<  endl;
    }

    oss_out << '+' << setw(16) << setfill('-') << right << '+' << setw(12) << setfill('-') << right << '+'  << setw(11) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;


    // vystup na stdout nebo do souboru
    Simulation::Output(oss_out.str());
}
