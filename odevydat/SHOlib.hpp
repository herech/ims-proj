///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    SHOlib.hpp                                                 ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef SHO_LIBRARY_INCLUDED
#define SHO_LIBRARY_INCLUDED


/**
 * Tento soubor vytvari knihovnu SHOlib
 * - uzavateli staci importovat (includovat) pouze tento hlavickovy soubor
 * - jsou zde zadefinovany konstanty pouzivane knihovnou
 * - jsou zde importovany hlavickove soubory pro praci s knihovnou
 */


/**
 * Zdroje casti kodu
 *
 * - zdroj wrapperu funkci
 * http://www.newty.de/fpt/callback.html#example1
 *
 * - zdroj iteratoru
 * http://stackoverflow.com/questions/12702561/c-iterate-through-vector-using-for-loop
 */


// konstanty pro praci generatoru pseudonahodnych cisel (trida RandomGenerator)
// pozivame konstanty glibc (pouzite v GCC)
#define DEFAULT_MULTIPLIER        1103515245  // defaultni nasobitel nahodneho cisla
#define DEFAULT_INCREMENT         12345       // defaultni prirustek nahodneho cisla
// modulo/RAND_MAX je nastaveno na UINT_MAX (2^32)
// seminko je defaultne spocitano z funkce casu


// konstanty pro praci obsluznych linek (trida Store)
#define FRONT_PRIORITY            1000        // priorita udalosti vytahnutych z fronty
#define GENERATOR_PRIORITY        10000       // priorita generovani transakci
#define DEFAULT_PROCESS_PRIORITY  0           // vychozi priorita procesu
#define INFINITE_CAPACITY         0           // definice nekonecne kapacity


// Importovani vsech hlavickovych souboru pro praci s knihovnou
#include "Stat.hpp"
#include "RandomGenerator.hpp"
#include "CalendarOfEvents.hpp"
#include "Event.hpp"
#include "Queue.hpp"
#include "Simulation.hpp"
#include "Store.hpp"
#include "Transaction.hpp"
#include "Histogram.hpp"


#endif // SHO_LIBRARY_INCLUDED
