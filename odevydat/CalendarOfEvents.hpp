///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    CalendarOfEvents.hpp                                       ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef CALENDAROFEVENT_HPP_INCLUDED
#define CALENDAROFEVENT_HPP_INCLUDED


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


using namespace std;


/**
 * Trida aktivacniho zaznamu udalosti, ktery ukladame do kalendare udalosti
 */
class ActivationRecord {
    public:
        void* event_object;                  // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
        void (*next_event_function)(void*);  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
        double activation_time;              // aktivacni cas udalosti
        int priority;                        // priorita udalosti
        void (*last_event_function)(void*);  // ukazatel na statickou obalovaci metodu, ve ktere se vytvari aktivacni zaznam
};


/**
 * Staticka trida kalendare udalosti
 */
class CalendarOfEvents {
    public:
        // staticka datova struktura uchovavajici seznam aktivacnich zaznamu udalosti
        static vector<ActivationRecord> activation_records;

        /**
         * Staticka metoda pro odstraneni vsech aktivacnich zaznamu z kalendare
         */
        static void clearActivationRecords();

        /**
         * Staticka metoda pro vlozeni aktivacniho zaznamu do kalendare
         * - kalendar je vzdy serazeny podle aktivacniho casu a priority udalosti
         * - pri vkladani hleda a vklada na spravne misto
         * @param  act_rec  Objekt ActivationRecord obsahujici aktivacni zaznam
         */
        static void addActivationRecord(ActivationRecord act_rec);

        /**
         * Staticka metoda pro odstraneni vsech aktivacnich zaznamu vztahujicich se k dane transakci
         * (zruseni transakce)
         * @param  *obj  Ukazatel na objekt transakce (dedi z tridy Transaction)
         */
        static void removeActRecByTransactObj(void* obj);

        /**
         * Staticka metoda pro odstraneni aktivacniho zaznamu naplanovaneho danou udalosti
         * - pro odstraneni vsech aktivacniho zaznamu naplanovaneho danou udalosti je treba volat v cyklu
         * @param   *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ve ktere byl aktivacni zaznam vytvoren
         * @return   void*                    Ukazatel na objekt transakce, jehoz metoda vytvorila aktivacni zaznam
         */
        static void* removeActRecByEventAndReturnTransactObj(void (*last_event_function_ptr)(void*));

        static void* removeActRecByEventAndTransactObj(void (*last_event_function_ptr)(void*), void* obj);

        static ActivationRecord removeActRecByEvent(void (*last_event_function_ptr)(void*));

    private:
        // alias pro tridu CalendarOfEvents pouzitelny uvnitr teto tridy
        typedef CalendarOfEvents self;
};


#endif // CALENDAROFEVENT_HPP_INCLUDED
