///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Store.cpp                                                  ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


/**
 * Zdroj iteratoru
 * http://stackoverflow.com/questions/12702561/c-iterate-through-vector-using-for-loop
 */


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>

#include <sstream>


using namespace std;


CurrentlyServicedTransaction::CurrentlyServicedTransaction (void* object_ptr, double service_time_begin)
{
    this->object_ptr = object_ptr;
    this->service_time_begin = service_time_begin;
}

// inicializace staticke datove struktury uchovavajici seznam vsech obsluznych linek v programu
vector<Store*> Store::all_stores;

/**
 * Konstruktor tridy Store
 * @param  name          Jmeno obsluzne linky
 * @param  capacity      Aktualni kapacita obsluzne linky
 * @param  max_capacity  Maximalni kapacita obsluzne linky
 */
Store::Store (string name, int capacity, int max_capacity)
{
    // aktualni kapacita obsluzne linky nemuze mit zapornou hodnotu
    if (capacity < 0)
    {
        warnx("Varování: kapacita skladu [%s] zadána jako záporná hodnota [%d]", name.c_str(), capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // maximalni kapacita obsluzne linky nemuze mit zapornou hodnotu
    if (max_capacity < 0)
    {
        warnx("Varování: maximální kapacita skladu [%s] zadána jako záporná hodnota [%d]", name.c_str(), max_capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // maximalni kapacita obsluzne linky nemuze byt mensi nez aktualni kapacita obsluzne linky
    if (capacity > max_capacity && max_capacity != INFINITE_CAPACITY)
    {
        warnx("Varování: kapacita skladu [%s] zadána jako hodnota [%d] je větší než hodnota maximální kapacity [%d]", name.c_str(), capacity, max_capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    self::all_stores.push_back(this);  // prida tuto frontu do seznamu vsech obsluznych linek

    this->capacity = capacity;          // aktualni kapacita obsluzne linky
    this->default_capacity = capacity;  // vychozi kapacita obsluzne linky
    this->max_capacity = max_capacity;  // maximalni kapacita obsluzne linky
    this->name = name;                  // jmeno obsluzne linky

    Queue *queue_1 = new Queue();             // vytvorim defaultni frontu
    queue_1->setName("Queue \"queue_1\" of Store: " + this->name); // nastavim ji jmeno
    (this->queue_vector).push_back(queue_1);  // vlozim prvni frontu do seznamu front teto obsluzne linky - prvni vlozena ma nejvyssi prioritu atd.
}


/**
 * Pretizeny konstruktor tridy Store
 * - explicitne nastavujeme frontu
 * @param   name                    Jmeno obsluzne linky
 * @param  *highest_priority_front  Ukazatel na frontu pro tuto obsluznou linku
 * @param   capacity                Aktualni kapacita obsluzne linky
 * @param   max_capacity            Maximalni kapacita obsluzne linky
 */
Store::Store (string name, Queue *highest_priority_front, int capacity, int max_capacity)
{
    // aktualni kapacita obsluzne linky nemuze mit zapornou hodnotu
    if (capacity < 0)
    {
        warnx("Varování: kapacita skladu [%s] zadána jako záporná hodnota [%d]", name.c_str(), capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // maximalni kapacita obsluzne linky nemuze mit zapornou hodnotu
    if (max_capacity < 0)
    {
        warnx("Varování: maximální kapacita skladu [%s] zadána jako záporná hodnota [%d]", name.c_str(), max_capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // maximalni kapacita obsluzne linky nemuze byt mensi nez aktualni kapacita obsluzne linky
    if (capacity > max_capacity && max_capacity != INFINITE_CAPACITY)
    {
        warnx("Varování: kapacita skladu [%s] zadána jako hodnota [%d] je větší než hodnota maximální kapacity [%d]", name.c_str(), capacity, max_capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    self::all_stores.push_back(this);  // prida tuto frontu do seznamu vsech obsluznych linek

    this->capacity = capacity;          // aktualni kapacita obsluzne linky
    this->default_capacity = capacity;  // vychozi kapacita obsluzne linky
    this->max_capacity = max_capacity;  // maximalni kapacita obsluzne linky
    this->name = name;                  // jmeno obsluzne linky

    (this->queue_vector).push_back(highest_priority_front);  // vlozime prvni frontu do seznamu front obsluzne linky - prvni vlozena ma nejvyssi prioritu atd.
}


/**
 * Metoda pro pridani fronty na konec seznamu front
 * - povazovana za frontu s nejmensi prioritou
 * @param  *new_queue  Ukazatel na frontu pro tuto obsluznou linku
 */
void Store::addQueue(Queue *new_queue)
{
    // ukazatel na frontu nesmi byt NULL
    if (new_queue == NULL)
    {
        warnx("Varování: chybný odkaz na vkládanou frontu");
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }
    else
    {
        (this->queue_vector).push_back(new_queue);  // vlozime frontu na konec seznamu front obsluzne linky
    }
}


/**
 * Metoda pro zabrani obsluzne linky
 * - pokud ji nemuze zabrat, jde do fronty
 * @param   quantity                 Pozadovana kapacita k zabrani
 * @param  *next_function_ptr        Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param  *object_ptr               Ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
 * @param   priority                 Priorita pro razeni ve fronte
 * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
 * @param  *queue_to_wait            Ukazatel na frontu u obsluzne linky
 */
void Store::Enter (int quantity, void (*next_function_ptr)(void*), void* object_ptr, int priority, void (*last_event_function_ptr)(void*), Queue* queue_to_wait)
{
    // nelze chtit zabrat vetsi kapacitu, nez je maximalni kapacita
    if (max_capacity != INFINITE_CAPACITY && max_capacity < quantity)
    {
        warnx("Varování: požadováno zabrání kapacity [%d] skladu [%s] větší než jeho maximální kapacita [%d]", quantity, name.c_str(), max_capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // pokud je obsluzna linka prazdna, nebo nema dostatecnou kapacitu, jde udalost do fronty
    if (Empty() || (capacity - quantity < 0))
    {
        QueueItem item;                                      // objekt vkladany do fronty
        item.quantity = quantity;                            // pozadovana kapacita k zabrani
        item.event_object = object_ptr;                      // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
        item.priority = priority;                            // priorita pro razeni ve fronte
        item.next_event_function = last_event_function_ptr;  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti (dalsi funkce (po vytahnuti z fronty) bude ta ze ktere jsem prisel)
        item.last_event_function = last_event_function_ptr;  // ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter

        // pokud nespecifikujeme frontu k cekani, pouzije se dafaultni fronta - prvni v seznamu front
        if (queue_to_wait == NULL)
        {
            // udalost se vlozi do fronty
            ((this->queue_vector).front())->Insert(item);
           // cout << "STORE: " << this->name << " transaction go to front to QUEUE: " << ((this->queue_vector).front())->getName() << endl;
        }
        // pokud frontu specifikujeme
        else
        {
            // iterator pres seznam front teto obsluzne linky
            auto it = begin (this->queue_vector);

            // projdeme fronty v seznamu front a hledame specifikovanou frontu
            for (; it != end (this->queue_vector); ++it)
            {
                // pokud ji najdeme
                if ( (*it) == queue_to_wait)
                {
                    // koncime hledani
                    break;
                }
            }

            // udalost vlozime do specifikovane fronty, nebo do posledni fronty (pokud specifikovanou frontu nenajdeme)
            (*it)->Insert(item);
           // cout << "STORE: " << this->name << " transaction go to front to QUEUE: " << (*it)->getName() << endl;
        }
    }
    // obluzna linka je schopna uspokojit pozadavek udalosti
    else
    {


        // inkrementujeme promennou pro kazdou frontu, aby vedela, ze transakce do ni nesla a mohla to zapocitat do svych statistik
        auto it = begin (this->queue_vector);
        for (; it != end (this->queue_vector); ++it)
        {
            (*it)->num_queuing_transaction++;
        }

        (this->stat_capacity_before)->Add(capacity); // vzorkujeme kapacitu pred operaci enter


        // snizime aktualni kapacitu obsluzneho zarizeni
        capacity -= quantity;
       // cout << "STORE: " << this->name << " seized by transaction" << endl;

        // naplanovani do kalendare
        ActivationRecord act_rec;                               // aktivacni zaznam udalosti
        act_rec.event_object = object_ptr;                      // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
        act_rec.next_event_function = next_function_ptr;        // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
        act_rec.activation_time = Simulation::Time();           // aktivacni cas udalosti
        act_rec.priority = priority;                            // priorita udalosti
        act_rec.last_event_function = last_event_function_ptr;  // ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter

        CalendarOfEvents::addActivationRecord(act_rec);         // vlozim aktivacni zaznam do kalendare udalosti


        // vlozime zaznam o aktualne obsluhovane transakci
        currently_serviced_transactions.push_back(new CurrentlyServicedTransaction(object_ptr, Simulation::Time()));

        incoming++;
        enter++;
        (this->stat_capacity_after)->Add(capacity); // vzorkujeme kapacitu po operaci enter
    }
}


/**
 * Metoda pro uvolneni obsluzne linky
 * - uvolni zadanou kapacitu
 * @param   quantity  Pocet, jakou kapacitu chceme uvolnit
 */
void Store::Leave (int quantity, void* object_ptr)
{
    // nemuzeme uvolnit vetsi kapacitu, nez je maximalni kapacita
    if (max_capacity != INFINITE_CAPACITY && (capacity + quantity) > max_capacity)
    {
        warnx("Varování: požadováno uvolnění kapacity na [%d] skladu [%s] větší, než jeho maximální kapacita [%d]", capacity + quantity, name.c_str(), max_capacity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // hledam transakci, ktera opousti linku v seznamu aktualne obsluhovanych transakci
    for (unsigned int i = 0; i < (this->currently_serviced_transactions).size(); i++)
    {
        if ((this->currently_serviced_transactions[i])->object_ptr == object_ptr)
        {
            (this->stat_service_time)->Add(Simulation::Time() - (this->currently_serviced_transactions[i])->service_time_begin); // zaradime dobu obsluhy do statistiky
            (this->currently_serviced_transactions).erase(begin (this->currently_serviced_transactions) + i);      // smazeme prvek
            outcoming++;
            break;
        }
    }

    leave++;

    capacity += quantity;         // navysime aktualni kapacitu
    int tmp_capacity = capacity;  // ulozime si aktualni kapacitu

    Queue *current_queue;                  // ukazatel na frontu ze seznamu front
    auto it = begin (this->queue_vector);  // iterator pres seznam front teto obsluzne linky

    // iterujeme pres vsechny fronty tohoto zarizeni
    for (; it != end (this->queue_vector); ++it)
    {
        // ulozime si ukazatel na frontu ze seznamu front
        current_queue = (*it);

        // prochazime obsah fronty ze seznamu front a vytahujeme udalosti z fronty, dokud mame dostatecnou kapacitu pro jejich uspokojeni
        while (!current_queue->Empty())
        {
            // zjistim prvni udalost ve fronte
            QueueItem item = (current_queue)->GetFirst();

            // pokud ma obsluzna linka dostatecnou kapacitu na uspokojeni pozadavku udalosti
            if (tmp_capacity - item.quantity >= 0)
            {
                // odecte se zabrana kapacita
                tmp_capacity -= item.quantity;

                // vytahnuta udalost se naplanuje do kalendare
                ActivationRecord act_rec;                                // aktivacni zaznam udalosti
                act_rec.event_object = item.event_object;                // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
                act_rec.next_event_function = item.next_event_function;  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
                act_rec.activation_time = Simulation::Time();            // aktivacni cas udalosti
                act_rec.priority = FRONT_PRIORITY;                       // priorita udalosti (vytahnute udalosti dostanou docasne zvysenou prioritu)
                act_rec.last_event_function = item.last_event_function;  // ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Activate

                CalendarOfEvents::addActivationRecord(act_rec);          // vlozim aktivacni zaznam do kalendare udalosti

                (current_queue)->DeleteFirst();                          // smazu prvni prvek z fronty
            }
            // obsluzna linka nema dostatecnou kapacitu na uspokojeni pozadavku udalosti
            else
            {
                // prestaneme prohledavat tuto frontu a posuneme se do dalsi fronty v seznamu front
                break;
            }
        }
    }
}


/**
 * Metoda pro zjisteni, zda je obsluzna linka prazdna
 * @return  true   Obsluzna linka je prazdna
 * @return  false  Obsluzna linka neni prazdna
 */
bool Store::Empty ()
{
    return (capacity == 0) ? true : false;
}

void Store::reinitializeAllStores()
{

    auto it = begin (self::all_stores);

    for (; it != end (self::all_stores); ++it)
    {
        (*it)->last_capacity = (*it)->capacity;
        (*it)->capacity = (*it)->default_capacity;
        ((*it)->currently_serviced_transactions).clear();
    }
}

/**
 * Metoda pro smazani zaznamenanych hodnot v ramci statistiky fronty
 */
void Store::Delete()
{
    (this->stat_service_time)->Delete();
    (this->stat_capacity_before)->Delete();
    (this->stat_capacity_after)->Delete();
    this->incoming = 0;
    this->outcoming = 0;
    this->enter = 0;
    this->leave = 0;
    this->last_capacity = 0;
}

/**
 * Metoda pro smazani/reinicializaci zaznamenanych hodnot ve statistice fronty
 */
void Store::Clear()
{
    // iterator pres seznam vsech storu systemu
    auto it = begin (Store::all_stores);

    // projdeme histogramy v seznamu storu a reinicializujeme cleny
    for (; it != end (Store::all_stores); ++it) {
        (*it)->Delete();
    }
}

/**
 * Metoda pro vypis statistik
 */
void Store::Output()
{
    /*(this->stat_service_time)->Output();
    (this->stat_capacity_before)->Output();
    cout << "INITIAL CAPACITY: " << this->default_capacity << " CURRENT_CPACITY: " << this->capacity << endl;
    cout << "INCOMING: " << incoming << endl;
    cout << "OUTCOMING: " << outcoming << endl;*/

    ostringstream tmp;

    tmp <<  " Initial capacity: " << default_capacity << " (currenty free " << last_capacity << ")";
    string tmp2 = tmp.str();

    ostringstream txt_out;  // datovy proud pro vystup

    // formatovani vypisu
    txt_out << '+' << setw(78) << setfill('-') << '+' <<  endl;
    txt_out << '|' << " STORE: " << setw(69) << setfill(' ') << left << name << '|' <<  endl;

    txt_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    txt_out << '|' << setw(77) << setfill(' ') << left << tmp2 << '|' << endl;

    txt_out << '+' << setw(39) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;

    txt_out << '|' << left << " Incoming transaction: " << setw(15) << setfill(' ') << incoming << right << '|';
    txt_out <<        left << " Number of Enter operation: " << setw(10) << setfill(' ') << enter<< right << '|' <<  endl;
    txt_out << '|' << left << " Outcoming transaction: " << setw(14) << setfill(' ') << outcoming  << right << '|';
    txt_out <<        left << " Number of Leave operation: " << setw(10) << setfill(' ') << leave << right << '|' <<  endl;

    txt_out << '+' << setw(39) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;

    txt_out << '|' << " Minimal free capacity: " << setw(53) << setfill(' ') << left << stat_capacity_after->getMinValue() << '|' <<  endl;
    txt_out << '|' << " Maximal free capacity: " << setw(53) << setfill(' ') << left << stat_capacity_before->getMaxValue() << '|' <<  endl;
    txt_out << '|' << " Average free capacity: " << setw(53) << setfill(' ') << left << (stat_capacity_before->getAverageValue() + stat_capacity_after->getAverageValue()) / 2.0 << '|' <<  endl;
    txt_out << '|' << " Free capacity deviation: " << setw(51) << setfill(' ') << left << (stat_capacity_before->getStdDeviation() + stat_capacity_after->getStdDeviation()) / 2.0 << '|' <<  endl;

    txt_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    txt_out << '|' << " Minimal service time: " << setw(54) << setfill(' ') << left << stat_service_time->getMinValue() << '|' <<  endl;
    txt_out << '|' << " Maximal service time: " << setw(54) << setfill(' ') << left << stat_service_time->getMaxValue() << '|' <<  endl;
    txt_out << '|' << " Average service time: " << setw(54) << setfill(' ') << left << stat_service_time->getAverageValue() << '|' <<  endl;
    txt_out << '|' << " Service time deviation: " << setw(52) << setfill(' ') << left << stat_service_time->getStdDeviation() << '|' <<  endl;

    txt_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;

    Simulation::Output(txt_out.str());

    // hledam transakci, ktera opousti linku v seznamu aktualne obsluhovanych transakci
    for (unsigned int i = 0; i < (this->queue_vector).size(); i++)
    {
        (this->queue_vector[i])->Output();
    }


    //Simulation::Output(txt_out.str());
}

/**
 * Metoda vrati hlavni frontu pro dany store
 */
Queue* Store::GetQueue() {
    return this->queue_vector[0];
}

/**
 * Metoda vrati volnou kapacitu daneho store
 */
int Store::GetCapacity() {
    return this->capacity;
}

/**
 * Metoda nastavi volnou kapacitu daneho store
 */
void Store::SetCapacity(int capacity) {
    this->capacity = capacity;
}
