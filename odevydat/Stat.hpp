///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Stat.hpp                                                   ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef STAT_HPP_INCLUDED
#define STAT_HPP_INCLUDED


#include "SHOlib.hpp"


#include <string>
#include <vector>
#include <limits>


using namespace std;

/**
 * Trida pro statistiky
 */
class Stat {
    private:
        int num_of_values = 0;                                // pocet vstupnich hodnot
        double max_value = numeric_limits<double>::lowest();  // maximalni zaznamenana hodnota
        double min_value = numeric_limits<double>::max();     // minimalni zaznamenana hodnota
        double sum_of_values = 0;                             // soucet vstupnich hodnot
        double sum_of_squares_of_values = 0;                  // soucet ctvercu vstupnich hodnot (nadruhou)
        string name;                                          // jmeno statistiky

    public:

        // staticka datova struktura uchovavajici seznam vsech statistik v systemu
        static vector<Stat*> all_stats;

        /**
         * Konstruktor tridy Stat
         * @param  name          Jmeno statistiky
         */
        Stat (string name);


        /**
         * Metoda pro vypis statistik
         */
        void Output();

        /**
         * Metoda pro pridani hodnoty do statisticky
         * @param  value  hodnota, ktera se zaznamena
         */
        void Add(double value);

        /**
         * Metoda pro smazani zaznamenanych hodnot a jejich statistik
         */
        void Delete();

        /**
         * Metoda pro smazani/reinicializaci zaznamenanych hodnot ve statistice
         */
        static void Clear();

        double getMaxValue();

        double getMinValue();

        double getNumOfValues();

        double getAverageValue();

        double getStdDeviation();

};

#endif // STAT_HPP_INCLUDED
