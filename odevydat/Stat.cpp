///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Stat.cpp                                                   ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <iomanip>
#include <string>
#include <functional>
#include <vector>
#include <err.h>

#include <iostream>
#include <fstream>
#include <limits>
#include <sstream>
#include <math.h>

using namespace std;

// inicializace staticke datove struktury uchovavajici seznam vsech statistik v systemu
vector<Stat*> Stat::all_stats;

/**
 * Konstruktor tridy Stat
 * @param  name          Jmeno statistiky
 */
Stat::Stat (string name)
{
    this->name = name;
    Stat::all_stats.push_back(this);   // prida tuto statistiku do seznamu vsech statistik
}

/**
 * Metoda pro vypis statistik
 */
void Stat::Output()
{

    //http://mathforum.org/library/drmath/view/52820.html
    double deviation = sqrt( ( this->num_of_values * this->sum_of_squares_of_values - this->sum_of_values * this->sum_of_values ) / ( this->num_of_values * (this->num_of_values - 1 )) );

    ostringstream txt_out;
    txt_out << '+' << setw(78) << setfill('-') << '+' <<  endl;
    txt_out << '|' << " STATISTIC: " << setw(65) << setfill(' ') << left << this->name << '|' <<  endl;
    txt_out << '+' << right << setw(78) << setfill('-') << '+' <<  endl;
    txt_out << '|' << " Number of records: " << setw(57) << setfill(' ') << left << this->num_of_values << '|' <<  endl;
    txt_out << '+' << setw(39) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;
    txt_out << '|' << left << " Minimal value: " << setw(22) << setfill(' ') << this->min_value << right << '|';
    txt_out <<        left << " Average value: " << setw(22) << setfill(' ') << (this->sum_of_values / this->num_of_values) << right << '|' <<  endl;
    txt_out << '|' << left << " Maximal value: " << setw(22) << setfill(' ') << this->max_value << right << '|';
    txt_out <<        left << " Standard deviation: " << setw(17) << setfill(' ') << deviation << right << '|' <<  endl;

    txt_out << '+' << setw(39) << setfill('-') << right << '+' << setw(39) << setfill('-') << '+' <<  endl;

    Simulation::Output(txt_out.str());
}

/**
 * Metoda pro pridani hodnoty do statisticky
 * @param  value  hodnota, ktera se zaznamena
 */
void Stat::Add(double value)
{

    this->num_of_values++;
    this->max_value = (value > this->max_value) ? value : this->max_value;
    this->min_value = (value < this->min_value) ? value : this->min_value;
    this->sum_of_values += value;
    this->sum_of_squares_of_values += (value * value);
}

/**
 * Metoda pro smazani/reinicializaci zaznamenanych hodnot ve statistice
 */
void Stat::Delete() {
    this->num_of_values = 0;
    this->max_value = numeric_limits<double>::lowest();
    this->min_value = numeric_limits<double>::max();
    this->sum_of_values = 0;
    this->sum_of_squares_of_values = 0;
}

double Stat::getMaxValue() {
    return this->max_value;
}

double Stat::getMinValue() {
    return this->min_value;
}

double Stat::getNumOfValues() {
    return this->num_of_values;
}

double Stat::getAverageValue() {
    return (this->sum_of_values / this->num_of_values);
}

double Stat::getStdDeviation() {
    return sqrt( ( this->num_of_values * this->sum_of_squares_of_values - this->sum_of_values * this->sum_of_values ) / ( this->num_of_values * (this->num_of_values - 1 )) );
}

/**
 * Staticka metoda pro reinicializaci vsech statistik
 */
void Stat::Clear() {
    // iterator pres seznam vsech statistik systemu
    auto it = begin (Stat::all_stats);

    // projdeme statistiky v seznamu histogramu a reinicializujeme cleny
    for (; it != end (Stat::all_stats); ++it) {
        (*it)->Delete();
    }
}
