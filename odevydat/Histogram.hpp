///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Histogram.hpp                                              ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef HISTOGRAM_HPP_INCLUDED
#define HISTOGRAM_HPP_INCLUDED


#include "SHOlib.hpp"


#include <string>
#include <vector>


using namespace std;


/**
 * Trida histogramu
 */
class Histogram {
    private:
        typedef Histogram self;         // alias pro tridu Histogram pouzitelny uvnitr teto tridy
        string name;                    // jmeno histogramu
        double start;                   // zacatek intervalu
        double step;                    // krok delici jednotlive tridy
        unsigned short num_of_classes;  // pocet trid histogramu
        vector<unsigned short> count;   // pocet prvku ve tride
        Stat *statHistogram;            // statistika pro histogram


    public:
        // staticka datova struktura uchovavajici seznam vsech histogramu v systemu
        static vector<Histogram*> all_histograms;

        /**
         * Konstruktor tridy Histogram
         * @param  name            Jmeno histogramu
         * @param  start           Zacatek intervalu
         * @param  step            Krok delici jednotlive tridy
         * @param  num_of_classes  Pocet trid histogramu
         */
        Histogram (string name, double start, double step, unsigned short num_of_classes);

        /**
         * Metoda pro vlozeni hodnoty do histogramu
         * @param  value  Vkladana hodnota
         */
        void Add (double value);

        /**
         * Metoda pro smazani obsahu histogramu
         * - hodnoty nastavene v kontruktoru zustanou stejne
         */
        void Delete();

        /**
         * Staticka metoda pro reinicializaci vsech histogramu
         */
        static void Clear();

        /**
         * Metoda pro vytisknuti statistik z histogramu
         */
        void Output();
};

#endif // HISTOGRAM_HPP_INCLUDED
