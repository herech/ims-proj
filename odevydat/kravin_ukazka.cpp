///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    kravin_ukazka.cpp                                          ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


/**
 * Zdroj wrapperu funkci
 * http://www.newty.de/fpt/callback.html#example1
 */


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


/**
 * Definice konstant pouzivanych v experimentu
 */
#define POCET_KRAV             100

// definice casovych udaju (v minutach)
#define ZACATEK_SIMULACE         0 * 60
#define KONEC_SIMULACE         168 * 60
#define DOBA_JIZDY               1 * 60
#define MIN_DOBA_NAKLADANI       1
#define MAX_DOBA_NAKLADANI       2

#define DOBA_VYTVORENI_MLEKA    15 * 60
#define STANDARDNI_DOBA_DOJENI   8
#define POMALA_DOBA_DOJENI      15

// definice kapacity linek
#define KAPACITA_AUTA           20


using namespace std;


/**
 * Vytvoreni obsluznych linek
 */
// obsluzne linky pro cast s autem
Store* Rampa =    new Store("Rampa", 1);
Store* Konvice =  new Store("Konvice");
Store* Kapacita = new Store("Kapacita");

// obsluzne linky pro cast s kravama
Store* Dojicky =  new Store("Dojicky", 5);


/**
 * Trida Auto
 * - dedi z tridy Transaction
 */
class Auto : public Transaction {
    public:
        // deklarace statickych obalovacich metod pro jednotlive udalosti
        static void start(void* object_ptr);
        static void nastavKapacituRampy(void* object_ptr);
        static void naRampe(void* object_ptr);
        static void jizda(void* object_ptr);
        static void cekaniNaKonvice(void* object_ptr);
        static void nakladaniKonvice(void* object_ptr);

        string jmeno;  // jmeno auta

        /**
         * Pretizeny konstruktor tridy Auto
         * - pridano vlozeni jmena auta
         */
        Auto(string jmeno, int priority = 0) : Transaction(priority) {
            this->jmeno = jmeno;
        }


    // definice metod pro jednotlive udalosti
    private:
        /**
         * Zacatek cyklu auta - prazdne auto
         * - pokusi se zabrat si rampu
         * - pokud ji zabere, pokracuje do stavu nastavKapacituRampy
         * - pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         */
        void start() {
            Enter(Rampa, Auto::nastavKapacituRampy, Auto::start);
        }

        /**
         * Auto ma zabranou rampu
         * - oznami rampe, kolik ma v sobe volnych mist
         * - pokracuje do stavu naRampe
         */
        void nastavKapacituRampy() {
            Leave(Kapacita, KAPACITA_AUTA);
            Activate(Auto::naRampe);
        }

        /**
         * Auto je na rampe a rampa vi kolik je v aute volnych mist
         * - pokud je v aute jeste nejake volne misto (!Empty(Kapacita)), auto prejde
         *   do stavu cekaniNaKonvice
         * - pokud v aute volne misto neni, auto opousti (uvolni) rampu a prejde
         *   do stavu jizda
         */
        void naRampe() {
            if (!Empty(Kapacita)) {
                Enter(Kapacita, Auto::cekaniNaKonvice, Auto::naRampe);
            }
            else {
                Leave(Rampa);
                Activate(Auto::jizda);
            }
        }

        /**
         * Auto je plne nalozeno konvicemi
         * - odveze konvice a vrati se (simulovano casovanym prechodem)
         * - prejde na zacatek sveho cyklu
         */
        void jizda() {
            Activate(Auto::start, DOBA_JIZDY);
            //cout << ">>> Jmeno AUTA: " << this->jmeno << " <<<" << endl;
        }

        /**
         * Auto ma volne misto na dalsi konvici
         * - pokusi se vzit konvici (zabrani obsluzne linky)
         * - pokud ji vezme, pokracuje do stavu nakladaniKonvice
         * - pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         */
        void cekaniNaKonvice() {
            Enter(Konvice, Auto::nakladaniKonvice, Auto::cekaniNaKonvice);
        }

        /**
         * Auto ma volne misto a zabral si konvici
         * - nalozi konvici a prejde do stavu naRampe, kde se rozhodne jestli ma jeste volne misto
         */
        void nakladaniKonvice() {
            Activate(Auto::naRampe, Uniform(MIN_DOBA_NAKLADANI, MAX_DOBA_NAKLADANI));
        }
};


/**
 * Definice statickych obalovacich metod pro jednotlive udalosti
 */
///////////////////////////////////////////////////////////////////////////////
void Auto::start(void* object_ptr) {                                         //
    Auto* self = (Auto*) object_ptr;                                         //
    self->start();                                                           //
}

void Auto::nastavKapacituRampy(void* object_ptr) {
    Auto* self = (Auto*) object_ptr;
    self->nastavKapacituRampy();
}

void Auto::naRampe(void* object_ptr) {
    Auto* self = (Auto*) object_ptr;
    self->naRampe();
}

void Auto::jizda(void* object_ptr) {
    Auto* self = (Auto*) object_ptr;
    self->jizda();
}

void Auto::cekaniNaKonvice(void* object_ptr) {
    Auto* self = (Auto*) object_ptr;
    self->cekaniNaKonvice();
}

void Auto::nakladaniKonvice(void* object_ptr) {
    Auto* self = (Auto*) object_ptr;                                         //
    self->nakladaniKonvice();                                                //
}                                                                            //
///////////////////////////////////////////////////////////////////////////////


/**
 * Trida Krava
 * - dedi z tridy Transaction
 */
class Krava : public Transaction {
    public:
        // deklarace statickych obalovacich metod pro jednotlive udalosti
        static void start(void* object_ptr);
        static void potrebaDojeni(void* object_ptr);
        static void pripravaNaDojeni(void* object_ptr);
        static void standardniDojeni(void* object_ptr);
        static void pomaleDojeni(void* object_ptr);
        static void dojeniDokonceno(void* object_ptr);


    // definice metod pro jednotlive udalosti
    private:
        /**
         * Zacatek cyklu kravy - nema mleko
         * - ceka dokud se ji mleko nevytvori a pak jde do stavu potrebaDojeni
         */
        void start() {
            Activate(Krava::potrebaDojeni, Exponencial(DOBA_VYTVORENI_MLEKA));
        }

        /**
         * Krava uz ma mleko a potrebuje podojit
         * - pokusi se zabrat si dojicku
         * - pokud ji zabere, pokracuje do stavu pripravaNaDojeni
         * - pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         */
        void potrebaDojeni() {
            Enter(Dojicky, Krava::pripravaNaDojeni, Krava::potrebaDojeni);
        }

        /**
         * Krava ma mleko a zabranou dojicku
         * - 10% krav se doji pomalu   -> jdou do stavu pomaleDojeni
         * - 90% krav se doji normalne -> jdou do stavu standardniDojeni
         */
        void pripravaNaDojeni() {
            if (Normalized() <= 0.1) {
                Activate(Krava::pomaleDojeni);
            }
            else {
                Activate(Krava::standardniDojeni);
            }
        }

        /**
         * Krava ma mleko, dojicku a vi, ze se bude dojit standardni dobu
         * - ceka az je podojena a prejde do stavu dojeniDokonceno
         */
        void standardniDojeni() {
            Activate(Krava::dojeniDokonceno, Exponencial(STANDARDNI_DOBA_DOJENI));
        }

        /**
         * Krava ma mleko, dojicku a vi, ze se bude dojit pomalu
         * - ceka az je podojena a prejde do stavu dojeniDokonceno
         */
        void pomaleDojeni() {
            Activate(Krava::dojeniDokonceno, Exponencial(POMALA_DOBA_DOJENI));
        }

        /**
         * Krava je podojena
         * - uvolni dojicku
         * - odevzda konvici s mlekem
         * - prejde na zacatek sveho cyklu
         */
        void dojeniDokonceno() {
            Leave(Dojicky);
            Leave(Konvice);
            Activate(Krava::start);
            //cout << ">>>>>>>>> Krava podojena a konvica odevzdana <<<<<<<<" << endl;
        }
};


/**
 * Definice statickych obalovacich metod pro jednotlive udalosti
 */
///////////////////////////////////////////////////////////////////////////////
void Krava::start(void* object_ptr) {                                        //
    Krava* self = (Krava*) object_ptr;                                       //
    self->start();                                                           //
}

void Krava::potrebaDojeni(void* object_ptr) {
    Krava* self = (Krava*) object_ptr;
    self->potrebaDojeni();
}

void Krava::pripravaNaDojeni(void* object_ptr) {
    Krava* self = (Krava*) object_ptr;
    self->pripravaNaDojeni();
}

void Krava::standardniDojeni(void* object_ptr) {
    Krava* self = (Krava*) object_ptr;
    self->standardniDojeni();
}

void Krava::pomaleDojeni(void* object_ptr) {
    Krava* self = (Krava*) object_ptr;
    self->pomaleDojeni();
}

void Krava::dojeniDokonceno(void* object_ptr) {
    Krava* self = (Krava*) object_ptr;                                       //
    self->dojeniDokonceno();                                                 //
}                                                                            //
///////////////////////////////////////////////////////////////////////////////


/**
 * Zacatek programu
 */
int main() {
    cout << endl << "****************** EXPERIMENT S KRAVINEM - UKAZKA - 100 KRAV ******************" << endl << endl;
    //Simulation::SetOutput("kravin_ukazka_100.dat");

    // FAZE INIT
    // inicializuje se simulace
    Simulation::Init(ZACATEK_SIMULACE, KONEC_SIMULACE);

    // vytvori se 100 krav
    for (int i = 0; i < POCET_KRAV; ++i) {
        (new Krava)->Activate(Krava::start);
    }

    // vytvori se dve auta
    (new Auto("Auto1"))->Activate(Auto::start);
    (new Auto("Auto2"))->Activate(Auto::start);


    // FAZE RUN
    // spusti se simulace
    Simulation::Run();


    // FAZE END
    // ukonci se simulace
    Simulation::End();

    // vypis statistik
    Dojicky->Output();
    Rampa->Output();
    Konvice->Output();

    // smazani statistik
    Simulation::Clear();
}
