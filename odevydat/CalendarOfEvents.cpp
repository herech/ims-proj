///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    CalendarOfEvents.cpp                                       ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


/**
 * Zdroj iteratoru
 * http://stackoverflow.com/questions/12702561/c-iterate-through-vector-using-for-loop
 */


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


using namespace std;


// inicializace staticke datove struktury uchovavajici seznam aktivacnich zaznamu udalosti
vector<ActivationRecord> CalendarOfEvents::activation_records;


/**
 * Staticka metoda pro odstraneni vsech aktivacnich zaznamu z kalendare
 */
void CalendarOfEvents::clearActivationRecords() {
    self::activation_records.clear();
}


/**
 * Staticka metoda pro vlozeni aktivacniho zaznamu do kalendare
 * - kalendar je vzdy serazeny podle aktivacniho casu a priority udalosti
 * - pri vkladani hleda a vklada na spravne misto
 * @param  act_rec  Objekt ActivationRecord obsahujici aktivacni zaznam
 */
void CalendarOfEvents::addActivationRecord(ActivationRecord act_rec) {
    // iterator pres aktivacni zaznamy
    auto it = begin (self::activation_records);

    // prochazime aktivacnimi zaznamy a hledame spravne misto, kam vlozit aktivacni zaznam
    for (; it != end (self::activation_records); ++it) {
        // pokud se rovna aktivacni cas vkladaneho zaznamu a zaznamu z kalendare
        if (it->activation_time == act_rec.activation_time) {
            // pokud ma vkladany zaznam vetsi prioritu nez zaznam z kalendare
            if (it->priority < act_rec.priority) {
                // ukoncime hledani a vlozime zaznam do kalendare zaznamu
                break;
            }
        }
        // pokud je aktivacni cas vkladaneho zaznamu mensi nez zaznamu z kalendare
        else if (it->activation_time > act_rec.activation_time) {
            // ukoncime hledani a vlozime zaznam do kalendare zaznamu
            break;
        }
    }

    // vlozime aktivacni zaznam na spravne misto
    self::activation_records.insert(it, act_rec);
}


/**
 * Staticka metoda pro odstraneni vsech aktivacnich zaznamu vztahujicich se k dane transakci
 * (zruseni transakce)
 * @param  *obj  Ukazatel na objekt transakce (dedi z tridy Transaction)
 */
void CalendarOfEvents::removeActRecByTransactObj(void* obj) {
    // mazani je ve while cyklu, protoze neni dobre mazat prvky objektu pres ktery iterujeme,
    // proto kdyz prvek smazeme, iterovani ukoncime a vytvorime si novy iterator
    while(true) {
        // iterator pres aktivacni zaznamy
        auto it = begin (self::activation_records);

        // prochazime aktivacnimi zaznamy
        for (; it != end (self::activation_records); ++it) {
            // objekt transakce z kalendare se rovna objektu predane transakce
            if (it->event_object == obj) {
                // smazeme tento zaznam a ukoncime FOR cyklus
                self::activation_records.erase(it);
                break;
            }
        }

        // pokud jsme dosli na konec ukoncime WHILE cyklus
        if (it == end(self::activation_records)) {
            break;
        }
    }
}


/**
 * Staticka metoda pro odstraneni aktivacniho zaznamu naplanovaneho danou udalosti
 * - pro odstraneni vsech aktivacniho zaznamu naplanovaneho danou udalosti je treba volat v cyklu
 * @param   *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ve ktere byl aktivacni zaznam vytvoren
 * @return   void*                    Ukazatel na objekt transakce, jehoz metoda vytvorila aktivacni zaznam
 */
void* CalendarOfEvents::removeActRecByEventAndReturnTransactObj(void (*last_event_function_ptr)(void*)) {
    // iterator pres aktivacni zaznamy
    auto it = begin (self::activation_records);

    // prochazime aktivacnimi zaznamy
    for (; it != end (self::activation_records); ++it) {
        // pokud se metoda z kalendare rovna predane metode
        if (it->last_event_function == last_event_function_ptr) {
            void* obj = it->event_object;        // ulozime ukaztel na objekt transakce
            self::activation_records.erase(it);  // smazeme aktivacni zaznam
            return obj;                          // vracime ukaztel na objekt transakce
        }
    }

    // neexistuje zaznam naplanovany danou udalosti - vracime NULL
    return NULL;
}

void* CalendarOfEvents::removeActRecByEventAndTransactObj(void (*last_event_function_ptr)(void*), void* obj) {
    // iterator pres aktivacni zaznamy
    auto it = begin (self::activation_records);

    // prochazime aktivacnimi zaznamy
    for (; it != end (self::activation_records); ++it) {
        // pokud se metoda z kalendare rovna predane metode
        if (it->last_event_function == last_event_function_ptr && it->event_object == obj) {
            void* obj = it->event_object;        // ulozime ukaztel na objekt transakce
            self::activation_records.erase(it);  // smazeme aktivacni zaznam
            return obj;                          // vracime ukaztel na objekt transakce
        }
    }

    // neexistuje zaznam naplanovany danou udalosti - vracime NULL
    return NULL;
}

ActivationRecord CalendarOfEvents::removeActRecByEvent(void (*last_event_function_ptr)(void*)) {
    // iterator pres aktivacni zaznamy
    auto it = begin (self::activation_records);

    // prochazime aktivacnimi zaznamy
    for (; it != end (self::activation_records); ++it) {
        // pokud se metoda z kalendare rovna predane metode
        if (it->last_event_function == last_event_function_ptr) {
            ActivationRecord act_rec = (*it);        // ulozime ukaztel na aktivacni zaznam
            self::activation_records.erase(it);      // smazeme aktivacni zaznam
            return act_rec;                          // vracime ukaztel na aktivacni zaznam
        }
    }

    ActivationRecord act_rec;
    act_rec.event_object = NULL;
    // neexistuje zaznam naplanovany danou udalosti - vracime NULL
    return act_rec;
}
