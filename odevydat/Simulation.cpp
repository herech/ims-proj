///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Simulation.cpp                                             ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>

#include <fstream>


using namespace std;


// inicializace tridni promennych
double Simulation::model_time_current = 0;   // aktualni modelovy cas
double Simulation::model_time_end = 0;       // koncovy modelovy cas
double Simulation::model_time_end_real = 0;  // skutecny koncovy modelovy cas
bool Simulation::cancel_simulation = false;  // priznak ukonceni simulace
string Simulation::stat_output = "STDOUT";   // typ vystupu statistik


/**
 * Staticka metoda pro inicializaci simulace
 * - nastavi pocatecni a koncovy modelovy cas simulace
 * @param  model_time_begin  Pocatecni modelovy cas simulace
 * @param  model_time_end    Koncovy modelovy cas simulace
 */
void Simulation::Init(double model_time_begin, double model_time_end) {
    self::model_time_current = model_time_begin;
    self::model_time_end = model_time_end;
}


/**
 * Staticka metoda pro beh simulace
 * - invokuje metodu nextEventAlgorithm()
 */
void Simulation::Run() {
    self::nextEventAlgorithm();
}


/**
 * Staticka metoda pro ziskani aktulniho modeloveho casu
 */
double Simulation::Time() {
    return self::model_time_current;
}


/**
 * Staticka metoda pro ziskani koncoveho modeloveho casu
 */
double Simulation::EndTime() {
    return self::model_time_end;
}


/**
 * Staticka metoda pro nastaveni priznaku zruseni simulace
 */
void Simulation::Cancel() {
    self::cancel_simulation = true;
}

/**
 * Metoda pro urceni aktualniho vystupu statistik
 * @param  stat_output  vystup - jmeno souboru nebo STDOUT
 */
void Simulation::SetOutput(string stat_output) {
    self::stat_output = stat_output;
}

/**
 * Metoda pro vraceni typu vystupu statistik
 * @return vraceni typu vystupu statistik - nazev souboru nebo STDOUT
 */
string Simulation::GetOutput() {
    return self::stat_output;
}


/**
 * Staticka metoda pro ukonceni simulace
 * - reinicializujeme promenne
 * - uvolni datove struktury
 * - reinicializuje obsluzne linky
 */
void Simulation::End() {
    self::model_time_end_real = self::model_time_current;
    self::model_time_current = 0;
    self::model_time_end = 0;
    self::cancel_simulation = false;

    // odstrani vsechny aktivacni zaznamy udalosti
    CalendarOfEvents::clearActivationRecords();

    // odstrani vsechny udalosti cekajici ve frontach
    Queue::clearAllQueueItems();

    // reinicializuje vsechny obsluzne linky
    Store::reinitializeAllStores();
}

/**
 * Staticka metoda pro ziskani skutecneho koncoveho modeloveho casu
 */
double Simulation::RealEndTime() {
    return self::model_time_end_real;
}


/**
 * Staticka metoda pro smazani (reinicializaci) statistik
 */
void Simulation::Clear() {
    // reinicializuje histogramy
    Histogram::Clear();

    // reinicialiyace statistik
    Stat::Clear();

    // reinicializuje statistiky front
    Queue::Clear();

    // reinicializace statistik storu
    Store::Clear();
}


/**
 * Staticka metoda pro spusteni next event algoritmu
 * - provadi udalosti naplanovane v kalendari udalosti
 */
void Simulation::nextEventAlgorithm() {
    // cykli dokud v kalendari udalosti existuji nejake aktivacni zaznamy a dokud neni nastaven priznak pro konec simulace
    while (!CalendarOfEvents::activation_records.empty() && self::cancel_simulation == false) {
        // vybere prvni zaznam z kalendare udalosti
        ActivationRecord act_rec = CalendarOfEvents::activation_records.front();
        // odstrani z kalendare udalosti tento zaznam
        CalendarOfEvents::activation_records.erase(CalendarOfEvents::activation_records.begin());

        // pokud je aktivacni cas udalosti z kalendare vetsi, nez koncovy modelovy cas, ukoncime simulaci
        if (act_rec.activation_time > self::model_time_end) {
            return;
        }

       // cout << "-------" << endl;
       // cout << "TIME: " << model_time_current << endl;
       // cout << "ACT_TIME: " << act_rec.activation_time << endl;
       // cout << "PRIORITY: " << act_rec.priority << endl;
       // cout << "-------" << endl;

        self::model_time_current = act_rec.activation_time;  // nastavi modelovy cas na aktivacni cas udalosti
        act_rec.next_event_function(act_rec.event_object);   // provede naplanovanou udalost
    }
}

/**
 * Metoda pro vypis statistik
 */
void Simulation::Output(string output)
{

    if (Simulation::GetOutput() != "STDOUT")
    {
        ofstream file;
        file.open (Simulation::GetOutput(), ios::out | ios::app);
        if (file.is_open())
        {
            file << output;
            if (file.bad())
            {
                warnx("Chyba: do souboru: [%s] se nepodarilo zapsat data", (Simulation::GetOutput()).c_str());
            }
            file.close();
        }
        else
        {
            warnx("Chyba: soubor: [%s] se nepodarilo otevrit pro zapis", (Simulation::GetOutput()).c_str());
        }
    }
    else
    {
        cout << output << endl;
    }
}
