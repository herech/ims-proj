///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    automaty.cpp                                               ///
///   Datum:     listopad 2015                                              ///
///   Kódování:  UTF-8                                                      ///
///   Předmět:   Modelování a simulace (IMS)                                ///
///   Projekt:   Téma 1: Diskrétní simulátor řízený událostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhaňák                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


/**
 * Zdroj wrapperu funkci
 * http://www.newty.de/fpt/callback.html#example1
 */

#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>

/**
 * Definice konstant pouzivanych v experimentu
 */
// definice casovych udaju (v minutach)
#define ZACATEK_SIMULACE 0 * 60
#define KONEC_SIMULACE 24* 60 * 100// 12h
#define DOBA_MEZI_PRICHODY_HRACU 10
#define DOBA_MEZI_PORUCHAMI 5 * 60
#define TIMEOUT_CEKANI_START 2
#define TIMEOUT_CEKANI_END 5

#define MIN_DOBA_HRANI 15
#define MAX_DOBA_HRANI 30

#define MIN_DOBA_PORUCHY 3
#define MAX_DOBA_PORUCHY 4

#define DEN_V_MINUTACH 1440

// definice kapacity linek
#define KAPACITA_PRIZNAKU_PORUCHY 0
#define KAPACITA_AUTOMATU 5

//definice pravdepodobnosti
#define PRAVDEPODOBNOST_ZE_HRAC_BUDE_CEKAT_VE_FRONTE 0.7


using namespace std;

/**
 * Vytvoreni obsluznych linek
 */
Store* Automaty = new Store("Automaty", KAPACITA_AUTOMATU);
Store* PriznakPoruchy = new Store("Priznak poruchy", KAPACITA_PRIZNAKU_PORUCHY);
Stat *StatDobaVSystemu = new Stat("Doba v systemu");
Histogram *HistDobaVSystemu = new Histogram("Doba v systemu", 0, 2, 20);

Histogram *HistTimeoutyZaDen = new Histogram("Prumerny pocet hracu, kterym se nechtelo cekat a odesli", 0, 1, 20);
int pocetTimeoutuZaDenVTisiciDnech[100] = {0};

Histogram *HistProcentoPrumernePlaticichZaDen = new Histogram("Kolik procent hracu ze vsech prichozich hralo", 80, 1, 20);
int pocetPrumernePrichozichZaDen[100] = {0};
int pocetPrumerneOdchozichPlaticichZaDen[100] = {0};


/**
 * Trida Hrac reprezentuje transakci Hrace v systemu
 * - dedi z tridy Transaction
 */
class Hrac : public Transaction {

    public:
        // deklarace statickych obalovacich metod pro jednotlive udalosti
        static void start(void* object_ptr);
        static void hraniNaAutomatech(void* object_ptr);
        static void automatyObsazeny(void* object_ptr);
        static void cekaniNaVolneAutomaty(void* object_ptr);
        static void timeoutCekani(void* object_ptr);
        static void hraniNaAutomatechDokonceno(void* object_ptr);
        static void vychoziMistoTimeoutu(void* object_ptr);
        static void konec(void* object_ptr);
        static int pocet_hracu;

    // definice metod pro jednotlive udalosti
    private:
        bool platnost_timeoutu = false; // zapina nebo vypina platnost timeoutu
        double cas_prichodu_hrace;
        int index_do_poctu_prichozich_za_den;

        /**
         * Zacatek transakce hrace
         */
        void start() {
            pocet_hracu++;
            index_do_poctu_prichozich_za_den = ((int) (Simulation::Time() - ZACATEK_SIMULACE)) / ((int) DEN_V_MINUTACH);
            pocetPrumernePrichozichZaDen[index_do_poctu_prichozich_za_den]++;
            cas_prichodu_hrace = Simulation::Time();

            cout << "HRAC PRICHAZI" << endl;
            // pokud existuje porucha ukoncime traksakci
            if (!Empty(PriznakPoruchy)) {
                cout << "PORUCHA VYHODILA HRACE, KTERY PRISEL" << endl;
                // urcim misto kam pujdu a misto ze ktereho tam jdu
                Activate(Hrac::konec);
            }
            // pokud je volny alespon jeden automat, budeme na nem hrat
            else if (!Empty(Automaty)) {
                cout << "HRAC ZABRAL AUTOMAT" << endl;
                Enter(Automaty, Hrac::hraniNaAutomatech, Hrac::start);
            }
            // pokud neni volny zadny automat, budeme se v nasledujici udalosti rozhodovat co podnikneme
            else {
                cout << "AUTOMATY JSOU VSECHYN OBSAZNEY, PROVEDEME ROZHODNUTI CO DELAT" << endl;
                Activate(Hrac::automatyObsazeny, Hrac::start);
            }
        }

        /**
         * Simulace hrani na automatech
         */
        void hraniNaAutomatech() {
            platnost_timeoutu = false; // deaktivuji platnost timeoutu

            // pokud existuje porucha ukoncime traksakci
            if (!Empty(PriznakPoruchy)) {
            cout << "PORUCHA VYHODILA HRACE, KTERY CHTEL HRAT" << endl;
              // vratime zabrany hraci automat a ukoncime transakci
              Leave(Automaty);
              Activate(Hrac::konec);
            }
            // po urcitou dobu hrac hraje a pote dojde k udalosti dokonceni hranu
            else {
                cout << "HRAC HRAJE" << endl;
                pocetPrumerneOdchozichPlaticichZaDen[index_do_poctu_prichozich_za_den]++;
                Activate(Hrac::hraniNaAutomatechDokonceno, Hrac::hraniNaAutomatech, Uniform(MIN_DOBA_HRANI, MAX_DOBA_HRANI));
            }
        }

        /**
         * Hrani na automatech bylo dokonceno
         * - vratime zabrany automat a ukoncime transakci
         */
        void hraniNaAutomatechDokonceno() {
            cout << "HRANI DOKONCENO" << endl;
            Leave(Automaty);
            Activate(Hrac::konec);
        }

        /**
         * Hraci se v minulem kroku nepodarilo zabrat si hraci automat, nyni vi ze jsou automaty obsazeny
         */
        void automatyObsazeny() {
            // pokud existuje porucha ukoncime traksakci
            if (!Empty(PriznakPoruchy)) {
                cout << "PORUCHA VYHODILA HRACE, KTERY PRISEL" << endl;
                Activate(Hrac::konec);
            }
            // neni porucha, provedeme rozhodnuti o nasledujici akci
            else {
                // 40% hracu bude chtit cekat ve fronte
                if (Normalized() <= PRAVDEPODOBNOST_ZE_HRAC_BUDE_CEKAT_VE_FRONTE) {
                    cout << "HRAC CEKA VE FRONTE" << endl;
                    Activate(Hrac::cekaniNaVolneAutomaty, Hrac::automatyObsazeny);
                }
                // 60% hracu nechce cekat, a odchazi ze systemu
                else {
                    cout << "HRAC NECEKA VE FRONTE" << endl;
                    Activate(Hrac::konec, Hrac::automatyObsazeny);
                }
            }
        }

        /**
         * Hrac ceka na uvolneni automatu ve fronte, zaroven se ale spusti casovac max. doby cekani
         */
        void cekaniNaVolneAutomaty() {
            // pokud existuje porucha ukoncime traksakci
            if (!Empty(PriznakPoruchy)) {
                cout << "PORUCHA VYHODILA HRACE KTERY CHCTEL CEKAT NA AUTOMAT" << endl;
                Activate(Hrac::konec);
            }
            // porucha neni, budu cekat ve fronte a zaroven spustim casovac max. doby cekani ve fronte
            else {
                cout << "TIMEOUT NAPLANOVAN" << endl;
                platnost_timeoutu = true; // aktivuji platnost timeoutu
                // naplanuji timeout a nastavim mu jako vzhoyi misto ze ktereho vysel specialni pro timeout
                Activate(Hrac::timeoutCekani, Hrac::vychoziMistoTimeoutu, Uniform(TIMEOUT_CEKANI_START, TIMEOUT_CEKANI_END));
                // jdu cekat do fronty na volne hraci automaty
                Enter(Automaty, Hrac::hraniNaAutomatech, Hrac::cekaniNaVolneAutomaty);
            }
        }

        /**
         * Udalost vyprseni casovace
         */
        void timeoutCekani() {
            // pokud timeout vyprsel a transakce si do te doby nezabrala linku,
            // je timeout platny a danou transakci ukonci v danem miste
            if (platnost_timeoutu == true) {
                cout << "TIMEOUT VALIDNE VYPRSEL..." << endl;
                TerminateInPlace(Hrac::cekaniNaVolneAutomaty, Hrac::konec, this);
                pocetTimeoutuZaDenVTisiciDnech[((int) (Simulation::Time() - ZACATEK_SIMULACE)) / ((int) DEN_V_MINUTACH) ]++;
            }

            //EndTransaction();
            // ukoncime transakci
            //Activate(Hrac::konec);
        }

        /**
         * Koncovy stav transakce ve kterem opousti system
         */
        void konec() {
            cout << "HRAC ODESEL SE SYSTEMU" << endl;
            StatDobaVSystemu->Add(Simulation::Time() - cas_prichodu_hrace);
            HistDobaVSystemu->Add(Simulation::Time() - cas_prichodu_hrace);
            EndTransaction();
        }

};

int Hrac::pocet_hracu = 0;

/**
 * Definice statickych obalovacich metod pro jednotlive udalosti
 */
///////////////////////////////////////////////////////////////////////////////
void Hrac::start(void* object_ptr) {                                         //
    Hrac* self = (Hrac*) object_ptr;                                         //
    self->start();                                                           //
}

void Hrac::hraniNaAutomatech(void* object_ptr) {
    Hrac* self = (Hrac*) object_ptr;
    self->hraniNaAutomatech();
}

void Hrac::automatyObsazeny(void* object_ptr) {
    Hrac* self = (Hrac*) object_ptr;
    self->automatyObsazeny();
}

void Hrac::vychoziMistoTimeoutu(void* object_ptr) {
    //Hrac* self = (Hrac*) object_ptr;
    //self->EndTransaction();
}

void Hrac::hraniNaAutomatechDokonceno(void* object_ptr) {
    Hrac* self = (Hrac*) object_ptr;
    self->hraniNaAutomatechDokonceno();
}

void Hrac::cekaniNaVolneAutomaty(void* object_ptr) {
    Hrac* self = (Hrac*) object_ptr;
    self->cekaniNaVolneAutomaty();
}

void Hrac::timeoutCekani(void* object_ptr) {
    Hrac* self = (Hrac*) object_ptr;
    self->timeoutCekani();
}

void Hrac::konec(void* object_ptr) {
    Hrac* self = (Hrac*) object_ptr;                                         //
    self->konec();                                                           //
}                                                                            //
///////////////////////////////////////////////////////////////////////////////

/**
 * Trida Porucha reprezentuje transakci poruchy v systemu
 * - dedi z tridy Transaction
 */
class Porucha : public Transaction {

    public:
        // deklarace statickych obalovacich metod pro jednotlive udalosti
        static void start(void* object_ptr);
        static void poruchaOdstranena(void* object_ptr);
        static void konec(void* object_ptr);

    private:
        // definice metod pro jednotlive udalosti

        /**
         * Zacatek transakce poruchy
         */
        void start() {
            cout << "PORUCHA SE VYSKYTLA" << endl;

            // pridame priznak poruchy
            Leave(PriznakPoruchy);

            // zrusime transakci na danych mistech (at uz ve fronte nebo kalendari)
            TerminateInPlace(Hrac::start, Hrac::konec);
            TerminateInPlace(Hrac::automatyObsazeny, Hrac::konec);
            TerminateInPlace(Hrac::cekaniNaVolneAutomaty, Hrac::konec);
            TerminateInPlace(Hrac::hraniNaAutomatech, Hrac::hraniNaAutomatechDokonceno);
            // zrusim timeout na jeho vychozim miste kde se odpocitava a poslu jej zpet na toto misto, ktere reprezentuje prazdnou funkci - tim se zrusi
            TerminateInPlace(Hrac::vychoziMistoTimeoutu, Hrac::vychoziMistoTimeoutu);

            // za nejaky cas se porucha odstrani
            Activate(Porucha::poruchaOdstranena, Uniform(MIN_DOBA_PORUCHY, MAX_DOBA_PORUCHY));
        }

        /**
         * Porucha byla odstranena
         */
        void poruchaOdstranena() {
            // zabereme priznak poruchy = odstranime jej
            Enter(PriznakPoruchy, Porucha::konec, Porucha::poruchaOdstranena);
        }

        /**
         * Koncovy stav ve kterem transakce poruchy odchazi ze systemu
         */
        void konec() {
            cout << "PORUCHA KONEC" << endl;
            EndTransaction();
        }

};

/**
 * Definice statickych obalovacich metod pro jednotlive udalosti
 */
///////////////////////////////////////////////////////////////////////////////
void Porucha::start(void* object_ptr) {                                      //
    Porucha* self = (Porucha*) object_ptr;                                   //
    self->start();                                                           //
}

void Porucha::poruchaOdstranena(void* object_ptr) {
    Porucha* self = (Porucha*) object_ptr;
    self->poruchaOdstranena();
}

void Porucha::konec(void* object_ptr) {
    Porucha* self = (Porucha*) object_ptr;                                   //
    self->konec();                                                           //
}                                                                            //
///////////////////////////////////////////////////////////////////////////////

/**
 * Trida GeneratorHracu reprezentuje jedinou udalost v case, konkretne generovani noveho hrace
 * - dedi z tridy Event
 */
class GeneratorHracu : public Event {

    public:
        // deklarace staticke obalovaci metody pro jedinou udalosti
        static void generovani(void* object_ptr);
        GeneratorHracu(int priority = GENERATOR_PRIORITY) : Event(priority) {}

    private:
        bool prvni_volani = true; // priznak prvniho volani generatoru

        // definice jedine metody pro tvorbu transakci

        /**
         * Udalost se provadi periodicky a vytvari/planuje nove transakce
         */
        void generovani() {
            if (prvni_volani == false) {
                (new Hrac)->Activate(Hrac::start);
                cout << "GENERUJU HRACE" << endl;
            }

            Activate(GeneratorHracu::generovani, Exponencial(DOBA_MEZI_PRICHODY_HRACU));
            prvni_volani = false;
        }
};

/**
 * Definice staticke obalovaci metody pro jedinou udalost
 */
////////////////////////////////////////////////////////////////////////////////
void GeneratorHracu::generovani(void* object_ptr) {                           //
    GeneratorHracu* self = (GeneratorHracu*) object_ptr;                      //
    self->generovani();                                                       //
}                                                                             //
////////////////////////////////////////////////////////////////////////////////

/**
 * Trida GeneratorPoruch reprezentuje jedinou udalost v case, konkretne generovani poruchy
 * - dedi z tridy Event
 */
class GeneratorPoruch : public Event {

    public:
        // deklarace staticke obalovaci metody pro jedinou udalosti
        static void generovani(void* object_ptr);
        GeneratorPoruch(int priority = GENERATOR_PRIORITY + 1) : Event(priority) {}

    private:
        bool prvni_volani = true; // priznak prvniho volani generatoru

        // definice jedine metody pro tvorbu transakci

        /**
         * Udalost se provadi periodicky a vytvari/planuje nove transakce
         */
        void generovani() {
            if (prvni_volani == false) {
                (new Porucha)->Activate(Porucha::start);
                cout << "GENERUJU PORUCHU" << endl;
            }
            Activate(GeneratorPoruch::generovani, Exponencial(DOBA_MEZI_PORUCHAMI));
            prvni_volani = false;
        }
};

/**
 * Definice staticke obalovaci metody pro jedinou udalost
 */
////////////////////////////////////////////////////////////////////////////////
void GeneratorPoruch::generovani(void* object_ptr) {                          //
    GeneratorPoruch* self = (GeneratorPoruch*) object_ptr;                    //
    self->generovani();                                                       //
}                                                                             //
////////////////////////////////////////////////////////////////////////////////

/**
 * Zacatek programu
 */
int main() {
    // FAZE INIT
    // inicializuje se simulace
    /*Simulation::Init(ZACATEK_SIMULACE, KONEC_SIMULACE);

    // vytvorim Generator hracu
    (new GeneratorHracu)->Activate(GeneratorHracu::generovani);

    // vytvorim Generator poruch
    //(new GeneratorPoruch)->Activate(GeneratorPoruch::generovani);

    cout << "Generatory byly vytvoreny" <<endl;

    // FAZE RUN
    // spusti se simulace
    Simulation::Run();

    // FAZE END
    // ukonci se simulace
    Simulation::End();

    //Simulation::SetOutput("tmp");
    StatDobaVSystemu->Output();
    StatDobaVSystemu->Clear();

    Hrac::pocet_hracu = 0;

    // další simulace*/

    Simulation::Init(ZACATEK_SIMULACE, KONEC_SIMULACE);

    // vytvorim Generator hracu
    (new GeneratorHracu)->Activate(GeneratorHracu::generovani);

    // vytvorim Generator poruch
    (new GeneratorPoruch)->Activate(GeneratorPoruch::generovani);

    cout << "Generatory byly vytvoreny" <<endl;

    // FAZE RUN
    // spusti se simulace
    Simulation::Run();

    // FAZE END
    // ukonci se simulace
    cout << "POCET prichozich HRACU: " << Hrac::pocet_hracu << endl;
    cout << "KALENDAR: " << CalendarOfEvents::activation_records.size() << endl;
    Simulation::End();

    //Simulation::SetOutput("OUTPUT.TXT");
    StatDobaVSystemu->Output();

    Automaty->Output();

    //PriznakPoruchy->Output();
    HistDobaVSystemu->Output();

    for (int i = 0; i < 100; i++) {
        HistTimeoutyZaDen->Add(pocetTimeoutuZaDenVTisiciDnech[i]);
    }
    HistTimeoutyZaDen->Output();


    for (int i = 0; i < 100; i++) {
        //HistProcentoPrumernePlaticichZaDen->Add( ((double) pocetPrumerneOdchozichPlaticichZaDen[i] / (double) pocetPrumernePrichozichZaDen[i]) * 100);
        HistProcentoPrumernePlaticichZaDen->Add((((double) pocetPrumerneOdchozichPlaticichZaDen[i]) / ((double) pocetPrumernePrichozichZaDen[i])) * 100.0);
        /*if (pocetPrumerneOdchozichPlaticichZaDen[i] > pocetPrumernePrichozichZaDen[i]) {
            cerr << "_2_" << pocetPrumerneOdchozichPlaticichZaDen[i] << "  " << pocetPrumernePrichozichZaDen[i] << "__" << endl;
        }*/
    }
    HistProcentoPrumernePlaticichZaDen->Output();

    Simulation::Clear();
    Hrac::pocet_hracu = 0;

    for (int i = 0; i < 100; i++) {
       pocetTimeoutuZaDenVTisiciDnech[i] = 0;
    }

    for (int i = 0; i < 100; i++) {
       pocetPrumerneOdchozichPlaticichZaDen[i] = 0;
    }

    for (int i = 0; i < 100; i++) {
       pocetPrumernePrichozichZaDen[i] = 0;
    }


}
