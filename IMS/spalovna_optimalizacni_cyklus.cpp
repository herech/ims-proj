///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    spalovna_optimalizacni_cyklus.cpp                          ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


/**
 * Zdroj wrapperu funkci
 * http://www.newty.de/fpt/callback.html#example1
 */


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>
#include <stdlib.h>


/**
 * Definice konstant pouzivanych v experimentu
 */
// definice casovych udaju (v minutach)
#define ZACATEK_SIMULACE                     0 * 60
#define KONEC_SIMULACE                       90 * 24 * 60
#define DOBA_MEZI_PRIJEZDY_DODAVEK           2 * 60
#define DOBA_VYSYPANI                        1

#define MIN_DOBA_KLASICKEHO_SPALOVANI        5
#define MAX_DOBA_KLASICKEHO_SPALOVANI        7

#define MIN_DOBA_ZRYCHLENEHO_SPALOVANI       2
#define MAX_DOBA_ZRYCHLENEHO_SPALOVANI       3

#define DOBA_ZRYCHLENEHO_SPALOVANI           1 * 60
#define DOBA_CHALDNUTI_SPALOVACIHO_ZARIZENI  8 * 60

// definice kapacity linek
#define VOLNA_KAPACITA_SKLADISTE    100
#define ZABRANA_KAPACITA_SKLADISTE    0
#define KAPACITA_DODAVKY             20

#define MEZ_PRO_ZRYCHLENI            50

// definice priorit
#define PRIORITA_SPALOVANI           10


using namespace std;


/**
 * Vytvoreni statistik
 */
Stat *spaleno_zrychlene = new Stat("Spaleno zrychlenym spalovanim za den");
Stat *spaleno_standardne = new Stat("Spaleno standardnim spalovanim za den");
//unsigned int spaleno_zrychlene_count  [90 * 24 ] = {0};
//unsigned int spaleno_standardne_count [90 * 24 ] = {0};

unsigned int spaleno_zrychlene_count = 0;
unsigned int spaleno_standardne_count = 0;


/**
 * Vytvoreni obsluznych linek
 */
Store* Rampa = new Store("Rampa", 1);
Store* VolnaKapacitaSkladiste = new Store("VolnaKapacitaSkladiste", VOLNA_KAPACITA_SKLADISTE);

// vytvorim uzivatelske fronty pro skladiste ZabranaKapacitaSkladiste
// druha fronta bude ke skladisti prirazena ve fazi INIT ve funkci main()
Queue *fronta_normalniho_spalovani         = new Queue("Fronta spalovani");
Queue *fronta_rizeni_zrychleneho_spalovani = new Queue("Fronta rizeni zrychleneho spalovani");
// vlozime priorotni frontu do skladiste ZabranaKapacitaSkladiste
Store* ZabranaKapacitaSkladiste    = new Store("ZabranaKapacitaSkladiste", fronta_normalniho_spalovani, ZABRANA_KAPACITA_SKLADISTE);

Store* PriznakZrychlenehoSpalovani = new Store("PriznakZrychlenehoSpalovani", 0);

Histogram *HistDobaDodavekVSystemu = new Histogram("Doba dodavek v systemu", 0, 7, 20);

int celkem_pokusu_o_zabrani_rampy = 0;
int celkem_uspesnych_pokusu_o_zabrani_rampy = 0;

int celkem_pokusu_o_zabrani_volne_kapacity_skladiste = 0;
int celkem_uspesnych_pokusu_o_volne_kapacity_skladiste = 0;



/**
 * Trida Dodavka
 * - dedi z tridy Transaction
 */
class Dodavka : public Transaction {
    public:
        // deklarace statickych obalovacich metod pro jednotlive udalosti
        static void start(void* object_ptr);
        static void rampaZabrana(void* object_ptr);
        static void vysypaniMaterialu(void* object_ptr);
        static void konec(void* object_ptr);


    // definice metod pro jednotlive udalosti
    private:
        /**
         * Zacatek transakce dodavky - je plna materialu pro spaleni
         * - pokusi se zabrat si rampu pro vysypani
         * - pokud ji zabere, pokracuje do stavu rampaZabrana
         * - pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         */
         double cas_prijezdu_dodavky;

        void start() {
            cas_prijezdu_dodavky = Simulation::Time();

            celkem_pokusu_o_zabrani_rampy++;
            if (!Empty(Rampa)) {
                celkem_uspesnych_pokusu_o_zabrani_rampy++;
            }
            Enter(Rampa, Dodavka::rampaZabrana, Dodavka::start);
        }

        /**
         * Dodavka ma matrial a zabranou rampu
         * - zjisti zda je ve skladisti dost mista na vylozeni materialu
         *   (zabrani kapacity KAPACITA_DODAVKY obsluzne linky VolnaKapacitaSkladiste)
         * - pokud ji zabere, pokracuje do stavu vysypaniMaterialu
         * - pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         */
        void rampaZabrana() {
            celkem_pokusu_o_zabrani_volne_kapacity_skladiste++;
            if (VolnaKapacitaSkladiste->GetCapacity() >= KAPACITA_DODAVKY) {
                celkem_uspesnych_pokusu_o_volne_kapacity_skladiste++;
            }
            Enter(VolnaKapacitaSkladiste, KAPACITA_DODAVKY, Dodavka::vysypaniMaterialu, Dodavka::rampaZabrana);
        }

        /**
         * Dodavka ma matrial, zabranou rampu a ve skladisti je dost mista
         * - vysype material na spaleni a prejde do stavu konec
         */
        void vysypaniMaterialu() {
            Activate(Dodavka::konec, DOBA_VYSYPANI);
        }

        /**
         * Dodavaka vysypala material na spaleni
         * - uvolni vysypaci rampu
         * - skladisti oznami, ze vysypala material
         *   (z obslusne linky ZabranaKapacitaSkladiste uvolni kapacitu KAPACITA_DODAVKY)
         * - dodavka odchazi ze systemu
         */
        void konec() {
            Leave(Rampa);
            Leave(ZabranaKapacitaSkladiste, KAPACITA_DODAVKY);
          //  cout << "DODAVKA ODJIZDI" << endl;
            HistDobaDodavekVSystemu->Add(Simulation::Time() - cas_prijezdu_dodavky);

            EndTransaction();
        }
};


/**
 * Definice statickych obalovacich metod pro jednotlive udalosti
 */
///////////////////////////////////////////////////////////////////////////////
void Dodavka::start(void* object_ptr) {                                      //
    Dodavka* self = (Dodavka*) object_ptr;                                   //
    self->start();                                                           //
}

void Dodavka::rampaZabrana(void* object_ptr) {
    Dodavka* self = (Dodavka*) object_ptr;
    self->rampaZabrana();
}

void Dodavka::vysypaniMaterialu(void* object_ptr) {
    Dodavka* self = (Dodavka*) object_ptr;
    self->vysypaniMaterialu();
}

void Dodavka::konec(void* object_ptr) {
    Dodavka* self = (Dodavka*) object_ptr;                                   //
    self->konec();                                                           //
}                                                                            //
///////////////////////////////////////////////////////////////////////////////


/**
 * Trida Spalovani
 * - dedi z tridy Transaction
 */
class Spalovani : public Transaction {
    public:
        // deklarace statickych obalovacich metod pro jednotlive udalosti
        static void start(void* object_ptr);
        static void zvysVolnouKapacituSkladisteProZrychleneSpalovani(void* object_ptr);
        static void zvysVolnouKapacituSkladisteProKlasickeSpalovani(void* object_ptr);
        static void klasickeSpalovani(void* object_ptr);
        static void zrychleneSpalovani(void* object_ptr);

        /**
         * Pretizeny konstruktor tridy Spalovani
         * - nastavi zvysenou prioritu transakce
         */
        Spalovani(int priority = PRIORITA_SPALOVANI) : Transaction(priority) {
        }


    // definice metod pro jednotlive udalosti
    private:
        /**
         * Zacatek cyklu spalovani - prazdny kotel
         * - zjisti zda byl nastaven priznak rychleho spalovani
         * - pokud ano, pokusi se vzit si jednu jednotku materialu pro spaleni
         *   (zabrani obsluzne linky ZabranaKapacitaSkladiste)
         *   -- pokud ziska material prechazi do stavu zvysVolnouKapacituSkladisteProZrychleneSpalovani
         *   -- pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         * - pokud ne, pokusi se vzit si jednu jednotku materialu pro spaleni
         *   (zabrani obsluzne linky ZabranaKapacitaSkladiste)
         *   -- pokud ziska material prechazi do stavu zvysVolnouKapacituSkladisteProKlasickeSpalovani
         *   -- pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         */
        void start() {
            // rychle spalovani
            if (!Empty(PriznakZrychlenehoSpalovani)) {
                Enter(ZabranaKapacitaSkladiste, Spalovani::zvysVolnouKapacituSkladisteProZrychleneSpalovani, Spalovani::start, fronta_normalniho_spalovani);
            }
            // pomale spalovani
            else {
                Enter(ZabranaKapacitaSkladiste, Spalovani::zvysVolnouKapacituSkladisteProKlasickeSpalovani, Spalovani::start, fronta_normalniho_spalovani);
            }
        }

        /**
         * Kotel si vzal material pro spaleni
         * - oznami skladisti, ze si vzal material pro spaleni
         *   (uvolneni obsluzne linky VolnaKapacitaSkladiste)
         * - prejde do stavu zrychleneSpalovani
         */
        void zvysVolnouKapacituSkladisteProZrychleneSpalovani() {
            Leave(VolnaKapacitaSkladiste);
            Activate(Spalovani::zrychleneSpalovani);
        }

        /**
         * Kotel si vzal material pro spaleni
         * - oznami skladisti, ze si vzal material pro spaleni
         *   (uvolneni obsluzne linky VolnaKapacitaSkladiste)
         * - prejde do stavu klasickeSpalovani
         */
        void zvysVolnouKapacituSkladisteProKlasickeSpalovani() {
            Leave(VolnaKapacitaSkladiste);
            Activate(Spalovani::klasickeSpalovani);
        }

        /**
         * Kotel spaluje material - zvysenou rychlosti
         * - po jeho spaleni prechazi do pocatecniho stavu
         */
        void zrychleneSpalovani() {
            //++spaleno_zrychlene_count[ int((Simulation::Time() - ZACATEK_SIMULACE) / (24 * 60)) ];
            ++spaleno_zrychlene_count;
            Activate(Spalovani::start, Uniform(MIN_DOBA_ZRYCHLENEHO_SPALOVANI, MAX_DOBA_ZRYCHLENEHO_SPALOVANI));
           // cout << "///////SPALOVANI ZRYCHLENE END" << endl;
        }

        /**
         * Kotel spaluje material - stadardni rychlosti
         * - po jeho spaleni prechazi do pocatecniho stavu
         */
        void klasickeSpalovani() {
            //++spaleno_standardne_count[ int((Simulation::Time() - ZACATEK_SIMULACE) / (24 * 60)) ];
            ++spaleno_standardne_count;
            Activate(Spalovani::start, Uniform(MIN_DOBA_KLASICKEHO_SPALOVANI, MAX_DOBA_KLASICKEHO_SPALOVANI));
           // cout << "///////SPALOVANI KLASICKE END" << endl;
        }
};


/**
 * Definice statickych obalovacich metod pro jednotlive udalosti
 */
///////////////////////////////////////////////////////////////////////////////
void Spalovani::start(void* object_ptr) {                                    //
    Spalovani* self = (Spalovani*) object_ptr;                               //
    self->start();                                                           //
}

void Spalovani::zvysVolnouKapacituSkladisteProZrychleneSpalovani(void* object_ptr) {
    Spalovani* self = (Spalovani*) object_ptr;
    self->zvysVolnouKapacituSkladisteProZrychleneSpalovani();
}

void Spalovani::zvysVolnouKapacituSkladisteProKlasickeSpalovani(void* object_ptr) {
    Spalovani* self = (Spalovani*) object_ptr;
    self->zvysVolnouKapacituSkladisteProKlasickeSpalovani();
}

void Spalovani::zrychleneSpalovani(void* object_ptr) {
    Spalovani* self = (Spalovani*) object_ptr;
    self->zrychleneSpalovani();
}

void Spalovani::klasickeSpalovani(void* object_ptr) {
    Spalovani* self = (Spalovani*) object_ptr;                               //
    self->klasickeSpalovani();                                               //
}                                                                            //
///////////////////////////////////////////////////////////////////////////////


/**
 * Trida RizeniZrychlenehoSpalovani
 * - dedi z tridy Transaction
 */
class RizeniZrychlenehoSpalovani : public Transaction {
    public:
        // deklarace statickych obalovacich metod pro jednotlive udalosti
        static void start(void* object_ptr);
        static void nastavPriznakProZrychleneSpalovani(void* object_ptr);
        static void timeoutZrychlenehoSpalovani(void* object_ptr);
        static void konecZrychlenehoSpalovani(void* object_ptr);
        static void chladnutiSpalovacihoZarizeni(void* object_ptr);

        /**
         * Pretizeny konstruktor tridy RizeniZrychlenehoSpalovani
         * - nastavi vychozi prioritu transakce
         */
        RizeniZrychlenehoSpalovani(int priority = DEFAULT_PROCESS_PRIORITY) : Transaction(priority) {}


    // definice metod pro jednotlive udalosti
    private:
        /**
         * Zacatek cyklu rizeni zrychleneho spalovani - neni zapnuto, ale muze byt
         * - zrychlene spalovani se ma zapnout, pokud se ve skladisti nahromadi vic
         *   materialu ke spaleni nez je mez zvolne hodnotou MEZ_PRO_ZRYCHLENI
         *   (zabrani kapacity MEZ_PRO_ZRYCHLENI obsluzne linky ZabranaKapacitaSkladiste)
         * - pokud ma dojit k zapnuti zrychleneho spalovani, prejde do stavu nastavPriznakProZrychleneSpalovani
         * - pokud ne, tak jde do fronty a pri vytazeni z fronty znovu provadi tuto metodu
         */
        void start() {
            Enter(ZabranaKapacitaSkladiste, MEZ_PRO_ZRYCHLENI, RizeniZrychlenehoSpalovani::nastavPriznakProZrychleneSpalovani, RizeniZrychlenehoSpalovani::start, fronta_rizeni_zrychleneho_spalovani);
        }

        /**
         * Zrychlene spalovani ma byt zapnuto
         * - vrati do skladiste material pro spaleni (chtel se pouze podivat, ne ho skutecne zabrat)
         * - nastavi priznak pro zrychlene spalovani (uvolneni obsluzne linky PriznakZrychlenehoSpalovani)
         * - prejde do stavu timeoutZrychlenehoSpalovani
         */
        void nastavPriznakProZrychleneSpalovani() {
          //  cout << "**************** ZACATEK REZIMU ZRYCHLENEHO SPALOVANI **************" << endl;
            Leave(ZabranaKapacitaSkladiste, MEZ_PRO_ZRYCHLENI);
            Leave(PriznakZrychlenehoSpalovani);
            Activate(RizeniZrychlenehoSpalovani::timeoutZrychlenehoSpalovani);
        }

        /**
         * Zrychlene spalovani muze bezet pouze po dobu DOBA_ZRYCHLENEHO_SPALOVANI
         * - po uplynuti teto doby prechazi do stavu konecZrychlenehoSpalovani
         */
        void timeoutZrychlenehoSpalovani() {
            Activate(RizeniZrychlenehoSpalovani::konecZrychlenehoSpalovani, DOBA_ZRYCHLENEHO_SPALOVANI);
        }

        /**
         * Zrychlene spalovani ma byt ukonceno
         * - odejme priznak zrychleneho spalovani (zabrani obsluzne linky PriznakZrychlenehoSpalovani)
         * - prechazi do stavu chladnutiSpalovacihoZarizeni
         */
        void konecZrychlenehoSpalovani() {
            Enter(PriznakZrychlenehoSpalovani, RizeniZrychlenehoSpalovani::chladnutiSpalovacihoZarizeni, RizeniZrychlenehoSpalovani::konecZrychlenehoSpalovani);
           // cout << "**************** KONEC REZIMU ZRYCHLENEHO SPALOVANI **************" << endl;
        }

        /**
         * Po dokonceni zrychleneho spalovani musi kotel chladnout po dobu DOBA_CHALDNUTI_SPALOVACIHO_ZARIZENI
         *   nez muze byt znovu zapnuto zrychlene spalovani
         * - po uplynuti doby DOBA_CHALDNUTI_SPALOVACIHO_ZARIZENI prechazi do pocatecniho stavu
         */
        void chladnutiSpalovacihoZarizeni() {
            Activate(RizeniZrychlenehoSpalovani::start, DOBA_CHALDNUTI_SPALOVACIHO_ZARIZENI);
        }
};

/**
 * Definice statickych obalovacich metod pro jednotlive udalosti
 */
////////////////////////////////////////////////////////////////////////////////////
void RizeniZrychlenehoSpalovani::start(void* object_ptr) {                        //
    RizeniZrychlenehoSpalovani* self = (RizeniZrychlenehoSpalovani*) object_ptr;  //
    self->start();                                                                //
}

void RizeniZrychlenehoSpalovani::nastavPriznakProZrychleneSpalovani(void* object_ptr) {
    RizeniZrychlenehoSpalovani* self = (RizeniZrychlenehoSpalovani*) object_ptr;
    self->nastavPriznakProZrychleneSpalovani();
}

void RizeniZrychlenehoSpalovani::timeoutZrychlenehoSpalovani(void* object_ptr) {
    RizeniZrychlenehoSpalovani* self = (RizeniZrychlenehoSpalovani*) object_ptr;
    self->timeoutZrychlenehoSpalovani();
}

void RizeniZrychlenehoSpalovani::konecZrychlenehoSpalovani(void* object_ptr) {
    RizeniZrychlenehoSpalovani* self = (RizeniZrychlenehoSpalovani*) object_ptr;
    self->konecZrychlenehoSpalovani();
}

void RizeniZrychlenehoSpalovani::chladnutiSpalovacihoZarizeni(void* object_ptr) {
    RizeniZrychlenehoSpalovani* self = (RizeniZrychlenehoSpalovani*) object_ptr;  //
    self->chladnutiSpalovacihoZarizeni();                                         //
}                                                                                 //
////////////////////////////////////////////////////////////////////////////////////


/**
 * Trida GeneratorDodavek
 * - dedi z tridy Event
 */
class GeneratorDodavek : public Event {
    public:
        // deklarace staticke obalovaci metody pro udalost
        static void generovani(void* object_ptr);

        /**
         * Pretizeny konstruktor tridy GeneratorDodavek
         * - nastavi zvysenou prioritu udalosti
         */
        GeneratorDodavek(int priority = GENERATOR_PRIORITY) : Event(priority) {}


    // definice jedine metody pro tvorbu transakci
    private:
        // znacka prvniho volani metody
        bool prvni_volani = true;

        /**
         * Metoda pro generovani transakci Dodavka
         * - vygeneruje a naplanuje do kalendare spusteni nove dodavky
         * - naplanuje do kalendare sve dalsi spusteni za dobu,
         *   kdy ma byt vygenerovana dalsi dodavka
         * - pri prvnim vola neplanuje dovaku, ale pouze sebe
         */
        void generovani() {
            if (prvni_volani == false) {
                (new Dodavka)->Activate(Dodavka::start);
               // cout << "GENERUJU DODAVKU" << endl;
            }

            Activate(GeneratorDodavek::generovani, Exponencial(DOBA_MEZI_PRIJEZDY_DODAVEK));
            prvni_volani = false;
        }
};


/**
 * Definice staticke obalovaci metody pro jedinou udalost
 */
///////////////////////////////////////////////////////////////////////////////
void GeneratorDodavek::generovani(void* object_ptr) {                        //
    GeneratorDodavek* self = (GeneratorDodavek*) object_ptr;                 //
    self->generovani();                                                      //
}                                                                            //
///////////////////////////////////////////////////////////////////////////////


/**
 * Zacatek programu
 */
int main(int argc, char* argv[]) {

    Transaction::setGeneratorSeed(0);
    Event::setGeneratorSeed(0);

    int kapacita_automatu = atoi( argv[1] );
    VolnaKapacitaSkladiste->SetCapacity(kapacita_automatu);

    // FAZE INIT
    // inicializuje se simulace
    Simulation::Init(ZACATEK_SIMULACE, KONEC_SIMULACE);
    // vlozime druhou mene prioritni frontu do skladiste ZabranaKapacitaSkladiste
    ZabranaKapacitaSkladiste->addQueue(fronta_rizeni_zrychleneho_spalovani);

    // vytvori se generator dodavek
    (new GeneratorDodavek)->Activate(GeneratorDodavek::generovani);

    // vytvori se transakce spalovani
    (new Spalovani)->Activate(Spalovani::start);

    // vytvori se transakce pro rizeni zrychleneho spalovani
    (new RizeniZrychlenehoSpalovani)->Activate(RizeniZrychlenehoSpalovani::start);


    // FAZE RUN
    // spusti se simulace
    Simulation::Run();


    // FAZE END
    // ukonci se simulace
    Simulation::End();

    /*for (int i = 0; i < 90 * 24; ++i) {
        spaleno_zrychlene->Add(spaleno_zrychlene_count[i]);
        spaleno_standardne->Add(spaleno_standardne_count[i]);
    }*/
    /*
    cout << "|KAPACITA| = " << kapacita_automatu << endl;
    if (VolnaKapacitaSkladiste->GetQueue()->GetIncoming() == 0) {
        cout << "HIT!" << endl;
    }
    cout << endl << "*** EXPERIMENT SE SPALOVNOU (MA OPTIMALIZOVANE PARAMETRY): mame spalovnu, ktera ma kapacitu pro 100 jednotek odpadu ***" << endl << endl;

    cout << "MODELOVY CAS: " <<  ZACATEK_SIMULACE << " - " << Simulation::RealEndTime() << " (v minutach)" << endl << endl;

    cout << "PRAVDEPODOBNOST, ZE PRIJETA DODAVKA NEBUDE CEKAT NA RAMPU: " <<  ((double) celkem_uspesnych_pokusu_o_zabrani_rampy) / ((double) celkem_pokusu_o_zabrani_rampy) << endl;
    cout << "PRAVDEPODOBNOST, ZE PRIJETA DODAVKA NEBUDE CEKAT NA VOLNOU KAPACITU SKLADISTE: " <<  ((double) celkem_uspesnych_pokusu_o_volne_kapacity_skladiste) / ((double) celkem_pokusu_o_zabrani_volne_kapacity_skladiste) << endl;

    cout << "POMER SPALENYCH JEDNOTEK ODPADU V (NORMALNIM : VYKONNEM REZIMU) V % Z CELKOVEHO OBJEMU SPALENYCH JEDNOTEK : (" << (((double) spaleno_standardne_count) / ((double) (spaleno_zrychlene_count + spaleno_standardne_count))) * 100.0 << "% : " << (((double) spaleno_zrychlene_count) / ((double) (spaleno_zrychlene_count + spaleno_standardne_count))) * 100.0 << "%)" << endl;
    cout << "CELKOVY POCET SPALENYCH JEDNOTEK ODPADU ZA (90 DNŮ / V PREPOCTU NA DEN): (" << spaleno_zrychlene_count + spaleno_standardne_count << " / " << (spaleno_zrychlene_count + spaleno_standardne_count) / 90.0 << ")" << endl << endl;

    // vypise statistiky
    Rampa->Output();
    VolnaKapacitaSkladiste->Output();
    //spaleno_zrychlene->Output();
    //spaleno_standardne->Output();

    fronta_normalniho_spalovani->Output();
    fronta_rizeni_zrychleneho_spalovani->Output();

    HistDobaDodavekVSystemu->Output();*/
    cout << kapacita_automatu << ",";
    cout << Rampa->GetQueue()->GetIncoming() << ",";
    cout << VolnaKapacitaSkladiste->GetQueue()->GetIncoming() << endl;

    // smaze statistiky
    Simulation::Clear();
    celkem_pokusu_o_zabrani_rampy = 0;
    celkem_uspesnych_pokusu_o_zabrani_rampy = 0;
    celkem_pokusu_o_zabrani_volne_kapacity_skladiste = 0;
    celkem_uspesnych_pokusu_o_volne_kapacity_skladiste = 0;
    spaleno_zrychlene_count = 0;
    spaleno_standardne_count = 0;
}
