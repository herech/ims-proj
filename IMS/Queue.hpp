///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Queue.hpp                                                  ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef QUEUE_HPP_INCLUDED
#define QUEUE_HPP_INCLUDED


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


using namespace std;


/**
 * Trida prvku fronty
 * - obsahuje potrebne informace pro zarazeni udalosti do fronty a take pro jeji naplanovani po opusteni fronty
 */
class QueueItem {
    public:
        void* event_object;                  // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
        void (*next_event_function)(void*);  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
        void (*last_event_function)(void*);  // ukazatel na statickou obalovaci metodu, ve ktere se vytvari prvek fronty
        int quantity;                        // pozadovana kapacita k zabrani
        int priority;                        // priorita udalosti ve fronte
        double start_time;                   // cas zarazeni udalosti do fronty
};


/**
 * Trida fronty
 */
class Queue {
    public:
        // staticka datova struktura uchovavajici seznam vsech front v systemu
        static vector<Queue*> all_queues;

        /**
         * Konstruktor tridy Queue
         * @param  name  Jmeno fronty
         */
        Queue(string name);

        /**
         * Pretizeny konstruktor tridy Queue
         * - nezadavame jmeno fronty pri inicializaci
         */
        Queue();

        /**
         * Metoda pro vlozeni prvku do fronty
         * - fronta je vzdy serazena podle priority udalosti a casu prichodu do fronty (FIFO priority Queue)
         * - pri vkladani hleda a vklada na spravne misto
         * @param  queue_item  Prvek fronty reprezentujici udalost, ktera ma jit cekat do fronty
         */
        void Insert(QueueItem queue_item);

        /**
         * Metoda pro zjisteni, zda je fronta prazdna
         * @return  true   Fronta je prazdna
         * @return  false  Fronta neni prazdna
         */
        bool Empty();

        /**
         * Metoda pro ziskani prvniho prvku z fronty
         * @return  QueueItem  Prvni prvek ve fronte
         */
        QueueItem GetFirst();

        /**
         * Metoda pro odstraneni prvniho prvku z fronty
         */
        void DeleteFirst();

        /**
         * Metoda pro nastaveni jmena fronty
         * @param  name  Jmeno fronty
         */
        void setName(string name);

        /**
         * Metoda pro ziskani jmena fronty
         * @return  string  Jmeno fronty
         */
        string getName();

        int getLength();

        /**
         * Staticka metoda pro smazani vsech udalosti cekajicich v nektere z fornt ve vektoru vsech front
         */
        static void clearAllQueueItems();

        /**
         * Staticka metoda pro reinicializaci statistik
         */
        static void Clear();

        /**
         * Metoda pro vypis statistik pro tuto frontu
         */
        void Output();

        static void* removeQueueItemByEvent(void (*last_event_function_ptr)(void*));

        static void* removeQueueItemByEventAndObj(void (*last_event_function_ptr)(void*), void* obj);

        unsigned int num_queuing_transaction = 0;

        /**
         * Metoda pro odstraneni nteho prvku z fronty
         */
        void DeleteNth(int n);

        /**
         * Metoda pro vraceni poctu prichozich transakci
         */
        unsigned int GetIncoming();

    private:
        typedef Queue self;                      // alias pro tridu Queue pouzitelny uvnitr teto tridy
        vector<QueueItem> waiting_transactions;  // datova struktura uchovavajici seznam udalosti cekajicich ve fronte
        unsigned int incoming;                   // prichozi udalosti do fronty
        unsigned int outcoming;                  // odchozi udalosti z fronty
        Stat *frontLengthBefore;                 // statistika delky fronty pred prichodem transakce do fronty
        Stat *frontLengthAfter;                  // statistika delky fronty po prichodu transakce do fronty
        Stat *frontTime;                         // statistika cekani ve fronte
        string name;                             // nazev fronty
};

#endif // QUEUE_HPP_INCLUDED
