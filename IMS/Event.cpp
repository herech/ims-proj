///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Event.cpp                                                  ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#include "SHOlib.hpp"


#include <iostream>
#include <string>
#include <err.h>


using namespace std;


// objekt nahodneho generatoru
RandomGenerator Event::generator;


/**
 * Konstruktor tridy Event
 * @param  priority  Priorita udalosti
 */
Event::Event(int priority) {
    this->priority = priority;
}


/**
 * Metoda pro naplanovani udalosti do kalendare (prida aktivacni zaznam udalosti)
 * @param  *next_event_function_ptr  Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param   time                     Aktivacni cas udalosti
 * @param   time_offset              Priznak zda je zadan casovy offset, nebo absolutni modelovy
 */
void Event::Activate(void (*next_event_function_ptr)(void*), double time, bool time_offset) {
    // pokud uzivatel zadal casovy offset oproti aktualnimu modelovemu casu
    if (time_offset == true) {
        // nelze planovat udalosti do minulosti
        if (time < 0.0) {
            warnx("Chyba: událost je naplánována na dřívější čas [%f], než je čas modelový [%f]", Simulation::Time() + time, Simulation::Time());
            warnx("Simulace bude ukončena...\n");
            Simulation::Cancel();
            return;
        }
        // spocita se absolutni modelovy cas pro zadany offset
        time = Simulation::Time() + time;
    }
    // pokud uzivatel zadal absolutni modelovy cas
    else {
        // nelze planovat udalosti do minulosti
        if (time < Simulation::Time()) {
            warnx("Chyba: událost je naplánována na dřívější čas [%f], než je čas modelový [%f]", time, Simulation::Time());
            warnx("Simulace bude ukončena...\n");
            Simulation::Cancel();
            return;
        }
    }

    // naplanovani do kalendare
    ActivationRecord act_rec;                               // aktivacni zaznam udalosti
    act_rec.event_object = (void*) this;                    // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
    act_rec.next_event_function = next_event_function_ptr;  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
    act_rec.activation_time = time;                         // aktivacni cas udalosti
    act_rec.priority = this->priority;                      // priorita udalosti
    act_rec.last_event_function = NULL;

    CalendarOfEvents::addActivationRecord(act_rec);         // vlozim aktivacni zaznam do kalendare udalosti
}


/**
 * Generuje pseudonahodna cisla rovnomerneho rozlozeni
 * - ve zvolenem intervalu <min_val, max_val)
 * @param   min_val  Dolni mez intervalu
 * @param   max_val  Horni mez intervalu
 * @return  double   Pseudonahodne cislo z intervalu <min_val, max_val)
 */
double Event::Uniform (double min_val, double max_val) {
    // vola metodu generatoru nahodnych cisel
    return self::generator.uniform(min_val, max_val);
}


/**
 * Generuje pseudonahodna cisla exponencialniho rozlozeni
 * @param   mean    Stredni hodnota
 * @return  double  Pseudonahodne cislo
 */
double Event::Exponencial(double mean) {
    // vola metodu generatoru nahodnych cisel
    return self::generator.exponencial(mean);
}


/**
 * Generuje pseudonahodna cisla gaussova (normalniho) rozlozeni
 * @param   mean                             Stredni hodnota
 * @param   deviation                        Smerodatna odchylka
 * @param   return_zero_instead_of_negative  Priznak, zda chceme pouze kladna cisla, nebo i zaporna
 * @return  double                           Pseudonahodne cislo
 */
double Event::Gaussian (double mean, double deviation, bool return_zero_instead_of_negative) {
    // vola metodu generatoru nahodnych cisel
    double rand_value = self::generator.gaussian(mean, deviation);

    // pokud necheme zaporna cisla
    if (return_zero_instead_of_negative == true) {
        // budeme vracet nulu
        rand_value = (rand_value < 0) ? 0 : rand_value;
    }

    return rand_value;
}


/**
 * Generuje normalizovana pseudonahodna cisla rovnomerneho rozlozeni
 * - v intervalu <0, 1)
 * @return  double  Pseudonahodne cislo z intervalu <0, 1)
 */
double Event::Normalized () {
    // vola metodu generatoru nahodnych cisel
    return self::generator.normalized();
}

 void Event::setGeneratorSeed(unsigned int seed) {
    self::generator.setSeed(seed);
 }
