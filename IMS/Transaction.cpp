///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Transaction.cpp                                            ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


using namespace std;


// objekt nahodneho generatoru
RandomGenerator Transaction::generator;


/**
 * Konstruktor tridy Transaction
 * @param  priority  Priorita transakce/procesu
 */
Transaction::Transaction(int priority) {
    this->priority = priority;
}


/**
 * Metoda pro naplanovani udalosti do kalendare (prida aktivacni zaznam udalosti)
 * @param  *next_event_function_ptr  Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param   time                     Aktivacni cas udalosti
 * @param   time_offset              Priznak zda je zadan casovy offset, nebo absolutni modelovy
 */
void Transaction::Activate(void (*next_event_function_ptr)(void*), double time, bool time_offset) {
    // pokud uzivatel zadal casovy offset oproti aktualnimu modelovemu casu
    if (time_offset == true) {
        // nelze planovat udalosti do minulosti
        if (time < 0.0) {
            warnx("Chyba: událost je naplánována na dřívější čas [%f], než je modelový [%f]", Simulation::Time() + time, Simulation::Time());
            warnx("Simulace bude ukončena...\n");
            Simulation::Cancel();
            return;
        }
        // spocita se absolutni modelovy cas pro zadany offset
        time = Simulation::Time() + time;
    }
    // pokud uzivatel zadal absolutni modelovy cas
    else {
        // nelze planovat udalosti do minulosti
        if (time < Simulation::Time()) {
            warnx("Chyba: událost je naplánována na dřívější čas [%f], než je modelový [%f]", time, Simulation::Time());
            warnx("Simulace bude ukončena...\n");
            Simulation::Cancel();
            return;
        }
    }

    // naplanovani do kalendare
    ActivationRecord act_rec;                               // aktivacni zaznam udalosti
    act_rec.event_object = (void*) this;                    // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
    act_rec.next_event_function = next_event_function_ptr;  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
    act_rec.activation_time = time;                         // aktivacni cas udalosti
    act_rec.priority = this->priority;                      // priorita udalosti
    act_rec.last_event_function = NULL;

    CalendarOfEvents::addActivationRecord(act_rec);         // vlozim aktivacni zaznam do kalendare udalosti
}


/**
 * Pretizena metoda pro naplanovani udalosti do kalendare (prida aktivacni zaznam udalosti)
 * - navic prida ukazatel na metodu, ze ktere je metoda Activate volana, to proto, aby bylo mozne pomoci teto metody, ktera reprezentuje dane misto,
 *   identifikovat aktivacni zaznam v kalendari udalosti (hodi se, pokud jej chceme smazat, nebo k nemu pristoupit. jinym zpusobem se k nemu nejsme schopni dostat)
 * @param  *next_event_function_ptr  Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Activate
 * @param   time                     Aktivacni cas udalosti
 * @param   time_offset              Priznak zda je zadan casovy offset, nebo absolutni modelovy
 */
void Transaction::Activate(void (*next_event_function_ptr)(void*), void (*last_event_function_ptr)(void*), double time, bool time_offset) {
    // pokud uzivatel zadal casovy offset oproti aktualnimu modelovemu casu
    if (time_offset == true) {
        // nelze planovat udalosti do minulosti
        if (time < 0.0) {
            warnx("Chyba: událost je naplánována na dřívější čas [%f], než je modelový [%f]", Simulation::Time() + time, Simulation::Time());
            warnx("Simulace bude ukončena...\n");
            Simulation::Cancel();
            return;
        }
        // spocita se absolutni modelovy cas pro zadany offset
        time = Simulation::Time() + time;
    }
    // pokud uzivatel zadal absolutni modelovy cas
    else {
        // nelze planovat udalosti do minulosti
        if (time < Simulation::Time()) {
            warnx("Chyba: událost je naplánována na dřívější čas [%f], než je modelový [%f]", time, Simulation::Time());
            warnx("Simulace bude ukončena...\n");
            Simulation::Cancel();
            return;
        }
    }

    // naplanovani do kalendare
    ActivationRecord act_rec;                               // aktivacni zaznam udalosti
    act_rec.event_object = (void*) this;                    // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
    act_rec.next_event_function = next_event_function_ptr;  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
    act_rec.activation_time = time;                         // aktivacni cas udalosti
    act_rec.priority = this->priority;                      // priorita udalosti
    act_rec.last_event_function = last_event_function_ptr;  // ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Activate

    CalendarOfEvents::addActivationRecord(act_rec);         // vlozim aktivacni zaznam do kalendare udalosti
}


/**
 * Metoda pro zabrani obsluzne linky
 * - pokud ji nemuze zabrat, jde do fronty
 * @param  *store                    Ukazatel na obsluznou linku
 * @param   quantity                 Pozadovana kapacita k zabrani
 * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
 */
void Transaction::Enter (Store* store, int quantity, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*)) {
    // nelze zabirat zapornou kapacitu
    if (quantity < 1) {
        warnx("Chyba: nelze zabírat zápornou kapacitu [%d]", quantity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // volame metodu Enter predane obsluzne linky
    store->Enter(quantity, function_ptr, (void*) this, this->priority, last_event_function_ptr);
}


/**
 * Přetížená metoda pro zabrani obsluzne linky
 * - pokud ji nemuze zabrat, jde do fronty
 * - zabirana kapacita (quantity) je defaultne 1
 * @param  *store                    Ukazatel na obsluznou linku
 * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
 */
void Transaction::Enter (Store* store, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*)) {
    int quantity = 1;

    // volame metodu Enter predane obsluzne linky
    store->Enter(quantity, function_ptr, (void*) this, this->priority, last_event_function_ptr);
}


/**
 * Přetížená metoda pro zabrani obsluzne linky
 * - pokud ji nemuze zabrat, jde do fronty
 * - explicitne urcujeme do ktere fronty u obsluzne linky se chceme zaradit
 * @param  *store                    Ukazatel na obsluznou linku
 * @param   quantity                 Pozadovana kapacita k zabrani
 * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
 * @param  *queue_to_wait            Ukazatel na frontu u obsluzne linky
 */
void Transaction::Enter (Store* store, int quantity, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*), Queue* queue_to_wait) {
    // nelze zabirat zapornou kapacitu
    if (quantity < 1) {
        warnx("Chyba: nelze zabírat zápornou kapacitu [%d]", quantity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // volame metodu Enter predane obsluzne linky
    store->Enter(quantity, function_ptr, (void*) this, this->priority, last_event_function_ptr, queue_to_wait);
}


/**
 * Přetížená metoda pro zabrani obsluzne linky
 * - pokud ji nemuze zabrat, jde do fronty
 * - zabirana kapacita (quantity) je defaultne 1
 * - explicitne urcujeme do ktere fronty u obsluzne linky se chceme zaradit
 * @param  *store                    Ukazatel na obsluznou linku
 * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
 * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
 * @param  *queue_to_wait            Ukazatel na frontu u obsluzne linky
 */
void Transaction::Enter (Store* store, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*), Queue* queue_to_wait) {
    int quantity = 1;

    // volame metodu Enter predane obsluzne linky
    store->Enter(quantity, function_ptr, (void*) this, this->priority, last_event_function_ptr, queue_to_wait);
}


/**
 * Metoda pro uvolneni obsluzne linky
 * - uvolni zadanou kapacitu
 * @param  *store     Ukazatel na obsluznou linku
 * @param   quantity  Pocet, jakou kapacitu chceme uvolnit
 */
void Transaction::Leave (Store* store, int quantity) {
    // nelze uvolnovat zapornou kapacitu
    if (quantity < 1) {
        warnx("Chyba: nelze uvolňovat zápornou kapacitu [%d]", quantity);
        warnx("Simulace bude ukončena...\n");
        Simulation::Cancel();
        return;
    }

    // volame metodu Leave predane obsluzne linky
    store->Leave(quantity, (void*) this);
}


/**
 * Metoda pro zjisteni, zda je obsluzna linka prazdna
 * @param   *store  Ukazatel na obsluznou linku
 * @return   true   Obsluzna linka je prazdna
 * @return   false  Obsluzna linka neni prazdna
 */
bool Transaction::Empty (Store* store) {
    return store->Empty();
}


/**
 * Generuje pseudonahodna cisla rovnomerneho rozlozeni
 * - ve zvolenem intervalu <min_val, max_val)
 * @param   min_val  Dolni mez intervalu
 * @param   max_val  Horni mez intervalu
 * @return  double   Pseudonahodne cislo z intervalu <min_val, max_val)
 */
double Transaction::Uniform (double min_val, double max_val) {
    // vola metodu generatoru nahodnych cisel
    return self::generator.uniform(min_val, max_val);
}


/**
 * Generuje pseudonahodna cisla exponencialniho rozlozeni
 * @param   mean    Stredni hodnota
 * @return  double  Pseudonahodne cislo
 */
double Transaction::Exponencial(double mean) {
    // vola metodu generatoru nahodnych cisel
    return self::generator.exponencial(mean);
}


/**
 * Generuje pseudonahodna cisla gaussova (normalniho) rozlozeni
 * @param   mean                             Stredni hodnota
 * @param   deviation                        Smerodatna odchylka
 * @param   return_zero_instead_of_negative  Priznak, zda chceme pouze kladna cisla, nebo i zaporna
 * @return  double                           Pseudonahodne cislo
 */
double Transaction::Gaussian (double mean, double deviation, bool return_zero_instead_of_negative) {
    // vola metodu generatoru nahodnych cisel
    double rand_value = self::generator.gaussian(mean, deviation);

    // pokud necheme zaporna cisla
    if (return_zero_instead_of_negative == true) {
        // budeme vracet nulu
        rand_value = (rand_value < 0) ? 0 : rand_value;
    }

    return rand_value;
}


/**
 * Generuje normalizovana pseudonahodna cisla rovnomerneho rozlozeni
 * - v intervalu <0, 1)
 * @return  double  Pseudonahodne cislo z intervalu <0, 1)
 */
double Transaction::Normalized () {
    // vola metodu generatoru nahodnych cisel
    return self::generator.normalized();
}


/**
 * Metoda pro zruseni transakci/procesu v kalendari a ve frontach
 * @param  *obj  Ukazatel na objekt transakce (dedi z tridy Transaction)
 */
void Transaction::Terminate(void* obj) {
    // zruseni z kalendare
    CalendarOfEvents::removeActRecByTransactObj(obj);
    // zruseni z fronty
}

void Transaction::TerminateInPlace(void (*last_event_function_ptr)(void*), void (*next_event_function_ptr)(void*), void* obj) {

    vector<void*> vector_of_terminated_objects_in_calendar;

    bool do_not_terminate_transaction;

    // zruseni z kalendare
    while(true) {

        do_not_terminate_transaction = false;

        void* transaction_obj;
        if (obj == NULL) {
            transaction_obj = CalendarOfEvents::removeActRecByEventAndReturnTransactObj(last_event_function_ptr);
        }
        else {
            transaction_obj = CalendarOfEvents::removeActRecByEventAndTransactObj(last_event_function_ptr, obj);
        }

        // pokud pro dane misto neexistuje objekt v kalendari, je vracen aktivacni zaznam s prazdnym objektem
        if (transaction_obj== NULL) {
            break;
        }

        // iterator pres jiz ukoncene transakce
        auto it = begin (vector_of_terminated_objects_in_calendar);

        // hledame jestli dany objekt jiz nebyl ukoncen (naplanovan do kalendare do ukoncovaci udalosti)
        for (; it != end (vector_of_terminated_objects_in_calendar); ++it) {
            // pokud objekt najdeme, tak transakci kterou predstavuje znovu neukoncime
            if ( (*it) == transaction_obj) {
                do_not_terminate_transaction = true;
                break;
            }
        }

        // objekt transakce, kterou ukoncuji si ulozim, abych jej 2x neukoncoval (napr. podruhe ve frontach)
        vector_of_terminated_objects_in_calendar.push_back(transaction_obj);

        // naplanujeme ukonceni transakce
        if (do_not_terminate_transaction == false) {
            ActivationRecord act_rec;                               // aktivacni zaznam udalosti
            act_rec.event_object = transaction_obj;   // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
            act_rec.next_event_function = next_event_function_ptr;  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
            act_rec.activation_time = Simulation::Time();           // aktivacni cas udalosti
            act_rec.priority = DEFAULT_PROCESS_PRIORITY;           // priorita udalosti
            act_rec.last_event_function = NULL;                     // nastavime ne null, protoze nas v tomto kontextu nezajima

            CalendarOfEvents::addActivationRecord(act_rec);         // vlozim aktivacni zaznam do kalendare udalosti
        }
    }

    // zruseni z fronty
    while(true) {

        do_not_terminate_transaction = false;
        void *object_transaction;
        if (obj == NULL) {
            object_transaction = Queue::removeQueueItemByEvent(last_event_function_ptr);
        }
        else {
            object_transaction = Queue::removeQueueItemByEventAndObj(last_event_function_ptr, obj);
        }

        // pokud pro dane misto neexistuje objekt ve frontě, je vracen prvek fronty s prazdnym objektem
        if (object_transaction == NULL) {
            break;
        }

        // iterator pres jiz ukoncene transakce
        auto it = begin (vector_of_terminated_objects_in_calendar);

        // hledame jestli dany objekt jiz nebyl ukoncen (naplanovan do kalendare do ukoncovaci udalosti)
        for (; it != end (vector_of_terminated_objects_in_calendar); ++it) {

            // pokud objekt najdeme, tak transakci kterou predstavuje znovu neukoncime
            if ( (*it) == object_transaction) {
                do_not_terminate_transaction = true;
                break;
            }
        }

        // naplanujeme ukonceni transakce
        if (do_not_terminate_transaction == false) {
            // naplanovani do kalendare
            ActivationRecord act_rec;                               // aktivacni zaznam udalosti
            act_rec.event_object = object_transaction;   // ukazatel na objekt ve kterem se nachazi metoda pro obsluhu udalosti
            act_rec.next_event_function = next_event_function_ptr;  // ukazatel na statickou obalovaci metodu pro obsluhu udalosti
            act_rec.activation_time = Simulation::Time();           // aktivacni cas udalosti
            act_rec.priority = DEFAULT_PROCESS_PRIORITY;           // priorita udalosti
            act_rec.last_event_function = NULL;                     // nastavime ne null, protoze nas v tomto kontextu nezajima

            CalendarOfEvents::addActivationRecord(act_rec);         // vlozim aktivacni zaznam do kalendare udalosti

        }
    }

}


/**
 * Metoda pro preruseni cekani transakce/procesu v kalendari udalosti podle metody odkud byla naplanovana
 * @param   *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ve ktere byl aktivacni zaznam vytvoren
 * @return   void*                    Ukazatel na objekt transakce, jehoz metoda vytvorila aktivacni zaznam
 */
void* Transaction::AbortWaiting(void (*last_event_function_ptr)(void*)) {
    return CalendarOfEvents::removeActRecByEventAndReturnTransactObj(last_event_function_ptr);
}

void Transaction::EndTransaction() {
    delete this;
}

 void Transaction::setGeneratorSeed(unsigned int seed) {
    self::generator.setSeed(seed);
 }
