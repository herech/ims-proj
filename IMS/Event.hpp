///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Event.hpp                                                  ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef EVENT_HPP_INCLUDED
#define EVENT_HPP_INCLUDED


#include "SHOlib.hpp"


using namespace std;


/**
 * Staticka trida udalosti
 * - pro uzivatele zapouzdruje praci s kalendarem udalosti a generatorem nahodnych cisel
 */
class Event {
    public:
        /**
         * Konstruktor tridy Event
         * @param  priority  Priorita udalosti
         */
        Event(int priority = DEFAULT_PROCESS_PRIORITY);

        /**
         * Metoda pro naplanovani udalosti do kalendare (prida aktivacni zaznam udalosti)
         * @param  *next_event_function_ptr  Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param   time                     Aktivacni cas udalosti
         * @param   time_offset              Priznak zda je zadan casovy offset, nebo absolutni modelovy
         */
        void Activate(void (*event_function_ptr)(void*), double time = 0, bool time_offset = true);

        /**
         * Generuje pseudonahodna cisla rovnomerneho rozlozeni
         * - ve zvolenem intervalu <min_val, max_val)
         * @param   min_val  Dolni mez intervalu
         * @param   max_val  Horni mez intervalu
         * @return  double   Pseudonahodne cislo z intervalu <min_val, max_val)
         */
        double Uniform (double min_val, double max_val);

        /**
         * Generuje pseudonahodna cisla exponencialniho rozlozeni
         * @param   mean    Stredni hodnota
         * @return  double  Pseudonahodne cislo
         */
        double Exponencial(double mean);

        /**
         * Generuje pseudonahodna cisla gaussova (normalniho) rozlozeni
         * @param   mean                             Stredni hodnota
         * @param   deviation                        Smerodatna odchylka
         * @param   return_zero_instead_of_negative  Priznak, zda chceme pouze kladna cisla, nebo i zaporna
         * @return  double                           Pseudonahodne cislo
         */
        double Gaussian (double mean, double deviation, bool return_zero_instead_of_negative = false);

        /**
         * Generuje normalizovana pseudonahodna cisla rovnomerneho rozlozeni
         * - v intervalu <0, 1)
         * @return  double  Pseudonahodne cislo z intervalu <0, 1)
         */
        double Normalized ();

        static void setGeneratorSeed(unsigned int seed);

    protected:
        int priority;                      // priorita udalosti
        static RandomGenerator generator;  // staticky objekt nahodneho generatoru

    private:
        // alias pro tridu Event pouzitelny uvnitr teto tridy
        typedef Event self;
};

#endif // EVENT_HPP_INCLUDED
