///////////////////////////////////////////////////////////////////////////////
///                                                                         ///
///   Soubor:    Transaction.hpp                                            ///
///   Datum:     listopad 2015                                              ///
///   Kodovani:  UTF-8                                                      ///
///   Predmet:   Modelovani a simulace (IMS)                                ///
///   Projekt:   Tema 1: Diskretni simulator rizeny udalostmi               ///
///   Autor:     Jan Herec                                                  ///
///              Pavel Juhanak                                              ///
///   Login:     xherec00                                                   ///
///              xjuhan01                                                   ///
///   Email:     <xherec00@stud.fit.vutbr.cz>                               ///
///              <xjuhan01@stud.fit.vutbr.cz>                               ///
///                                                                         ///
///////////////////////////////////////////////////////////////////////////////


#ifndef TRANSACTION_HPP_INCLUDED
#define TRANSACTION_HPP_INCLUDED


#include "SHOlib.hpp"


#include <iomanip>
#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <err.h>


using namespace std;


/**
 * Staticka trida transakce/procesu
 * - pro uzivatele zapouzdruje praci s kalendarem udalosti, obsluznymi linkami a generatorem nahodnych cisel
 */
class Transaction {
    public:
        /**
         * Konstruktor tridy Transaction
         * @param  priority  Priorita transakce/procesu
         */
        Transaction(int priority = DEFAULT_PROCESS_PRIORITY);

        /**
         * Metoda pro naplanovani udalosti do kalendare (prida aktivacni zaznam udalosti)
         * @param  *next_event_function_ptr  Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param   time                     Aktivacni cas udalosti
         * @param   time_offset              Priznak zda je zadan casovy offset, nebo absolutni modelovy
         */
        void Activate(void (*event_function_ptr)(void*), double time = 0, bool time_offset = true);


        /**
         * Pretizena metoda pro naplanovani udalosti do kalendare (prida aktivacni zaznam udalosti)
         * - navic prida ukazatel na metodu, ze ktere je metoda Activate volana, to proto, aby bylo mozne pomoci teto metody, ktera reprezentuje dane misto,
         *   identifikovat aktivacni zaznam v kalendari udalosti (hodi se, pokud jej chceme smazat, nebo k nemu pristoupit. jinym zpusobem se k nemu nejsme schopni dostat)
         * @param  *next_event_function_ptr  Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Activate
         * @param   time                     Aktivacni cas udalosti
         * @param   time_offset              Priznak zda je zadan casovy offset, nebo absolutni modelovy
         */
        void Activate(void (*event_function_ptr)(void*), void (*current_place_function_ptr)(void*), double time = 0, bool time_offset = true);

        /**
         * Metoda pro zabrani obsluzne linky
         * - pokud ji nemuze zabrat, jde do fronty
         * @param  *store                    Ukazatel na obsluznou linku
         * @param   quantity                 Pozadovana kapacita k zabrani
         * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
         */
        void Enter (Store* store, int quantity, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*));

        /**
         * Přetížená metoda pro zabrani obsluzne linky
         * - pokud ji nemuze zabrat, jde do fronty
         * - zabirana kapacita (quantity) je defaultne 1
         * @param  *store                    Ukazatel na obsluznou linku
         * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
         */
        void Enter (Store* store, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*));

        /**
         * Přetížená metoda pro zabrani obsluzne linky
         * - pokud ji nemuze zabrat, jde do fronty
         * - explicitne urcujeme do ktere fronty u obsluzne linky se chceme zaradit
         * @param  *store                    Ukazatel na obsluznou linku
         * @param   quantity                 Pozadovana kapacita k zabrani
         * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
         * @param  *queue_to_wait            Ukazatel na frontu u obsluzne linky
         */
        void Enter (Store* store, int quantity, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*), Queue* queue_to_wait);

        /**
         * Přetížená metoda pro zabrani obsluzne linky
         * - pokud ji nemuze zabrat, jde do fronty
         * - zabirana kapacita (quantity) je defaultne 1
         * - explicitne urcujeme do ktere fronty u obsluzne linky se chceme zaradit
         * @param  *store                    Ukazatel na obsluznou linku
         * @param  *function_ptr             Ukazatel na statickou obalovaci metodu pro obsluhu udalosti
         * @param  *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ze ktere je volana metoda Enter
         * @param  *queue_to_wait            Ukazatel na frontu u obsluzne linky
         */
        void Enter (Store* store, void (*function_ptr)(void*), void (*last_event_function_ptr)(void*), Queue* queue_to_wait);

        /**
         * Metoda pro uvolneni obsluzne linky
         * - uvolni zadanou kapacitu
         * @param  *store     Ukazatel na obsluznou linku
         * @param   quantity  Pocet, jakou kapacitu chceme uvolnit
         */
        void Leave (Store* store, int quantity = 1);

        /**
         * Metoda pro zjisteni, zda je obsluzna linka prazdna
         * @param   *store  Ukazatel na obsluznou linku
         * @return   true   Obsluzna linka je prazdna
         * @return   false  Obsluzna linka neni prazdna
         */
        bool Empty (Store* store);

        /**
         * Generuje pseudonahodna cisla rovnomerneho rozlozeni
         * - ve zvolenem intervalu <min_val, max_val)
         * @param   min_val  Dolni mez intervalu
         * @param   max_val  Horni mez intervalu
         * @return  double   Pseudonahodne cislo z intervalu <min_val, max_val)
         */
        double Uniform (double min_val, double max_val);

        /**
         * Generuje pseudonahodna cisla exponencialniho rozlozeni
         * @param   mean    Stredni hodnota
         * @return  double  Pseudonahodne cislo
         */
        double Exponencial(double mean);

        /**
         * Generuje pseudonahodna cisla gaussova (normalniho) rozlozeni
         * @param   mean                             Stredni hodnota
         * @param   deviation                        Smerodatna odchylka
         * @param   return_zero_instead_of_negative  Priznak, zda chceme pouze kladna cisla, nebo i zaporna
         * @return  double                           Pseudonahodne cislo
         */
        double Gaussian (double mean, double deviation, bool return_zero_instead_of_negative = false);

        /**
         * Generuje normalizovana pseudonahodna cisla rovnomerneho rozlozeni
         * - v intervalu <0, 1)
         * @return  double  Pseudonahodne cislo z intervalu <0, 1)
         */
        double Normalized ();

        /**
         * Metoda pro zruseni transakci/procesu v kalendari a ve frontach
         * @param  *obj  Ukazatel na objekt transakce (dedi z tridy Transaction)
         */
        void Terminate(void* obj);

        void TerminateInPlace(void (*last_event_function_ptr)(void*), void (*next_event_function_ptr)(void*), void* obj = NULL);

        /**
         * Metoda pro preruseni cekani transakce/procesu v kalendari udalosti podle metody odkud byla naplanovana
         * @param   *last_event_function_ptr  Ukazatel na statickou obalovaci metodu, ve ktere byl aktivacni zaznam vytvoren
         * @return   void*                    Ukazatel na objekt transakce, jehoz metoda vytvorila aktivacni zaznam
         */
        void* AbortWaiting(void (*last_event_function_ptr)(void*));

        static void setGeneratorSeed(unsigned int seed);

    protected:
        int priority;                      // priorita transakce/procesu
        static RandomGenerator generator;  // staticky objekt nahodneho generatoru

        void EndTransaction();

    private:
        // alias pro tridu Transaction pouzitelny uvnitr teto tridy
        typedef Transaction self;
};

#endif // TRANSACTION_HPP_INCLUDED
